import React, { useState, useEffect } from 'react';
import { Jumbotron, Card, CardDeck, Row, Col, Container } from 'react-bootstrap';
import MemberCard from './MemberCard';
import ToolCard from './ToolCard';
import { members } from './members';
import { tools } from './tools';
import { sources } from './sources';
import '../styles/aboutpage.css';

import gitlabPic from '../images/gitlab.svg'
import postmanPic from '../images/postman.svg'
import testPic from '../images/test.png'
import clipboardPic from '../images/clipboard.png'
import slideshowPic from '../images/slideshow.png'

const AboutPage = () => {
    document.title = "About";

    const [isLoading, setLoading] = useState(true);
    const [isFailure, setFailure] = useState(false);
    /* Contribution State */
    const [memberCommits, setMemberCommits] = useState([0, 0, 0, 0, 0]);
    const [memberIssues, setMemberIssues] = useState([0, 0, 0, 0, 0]);
    const [commitTotals, setCommitTotals] = useState(0);
    const [issueTotals, setIssueTotals] = useState(0);
    const memberTests = [13, 0, 0, 0, 2];
    const testTotals = memberTests[0] + memberTests[1] + memberTests[2] + memberTests[3] + memberTests[4];

    const ISSUES_API = "https://gitlab.com/api/v4/projects/27550527/issues";
    const COMMITS_API = "https://gitlab.com/api/v4/projects/27550527/repository/commits";

    const apiURL = process.env.REACT_APP_API_URL + '/test/';
 
    useEffect(() => {
        const getCommits = async () => {
            let commits = [0, 0, 0, 0, 0, 0];
            let xNextPage = "1";
            while (xNextPage) {
                const response = await fetch(COMMITS_API + '?per_page=100&page=' + xNextPage);
                const OUR_COMMITS = await response.json();
                OUR_COMMITS.forEach((commit) => {
                    let name = commit['author_name'];
                    if (name === "Gautham Gujjula") {
                        commits[0] += 1;
                    } else if (name === "Austin Dominguez") {
                        commits[1] += 1;
                    } else if (name === "Sang Pham") {
                        commits[2] += 1;
                    } else if (name === "Adithya Arunganesh") {
                        commits[3] += 1;
                    } else if (name === "Ryan Travers") {
                        commits[4] += 1;
                    }
                    commits[5] += 1;
                })
                xNextPage = response.headers.get('x-next-page')
            }
            setMemberCommits(commits);
            setCommitTotals(commits[5]);
        };
    
        const getIssues = async () => {
            let issues = [0, 0, 0, 0, 0, 0];
            /* to handle paginated response from gitlab */
            let xNextPage = "1";
            while (xNextPage) {
                const response = await fetch(ISSUES_API + '?per_page=100&page=' + xNextPage);
                const OUR_ISSUES = await response.json();
                OUR_ISSUES.forEach((issue) => {
                    let assignees = issue.assignees;
                    assignees.forEach((assignee) => {
                        let name = assignee.name;
                        /* update individual issues numbers */
                        if (name === "Gautham Gujjula") {
                            issues[0] += 1;
                        } else if (name === "Austin Dominguez") {
                            issues[1] += 1;
                        } else if (name === "Sang Pham") {
                            issues[2] += 1;
                        } else if (name === "Adithya Arunganesh") {
                            issues[3] += 1;
                        } else if (name === "Ryan Travers") {
                            issues[4] += 1;
                        }
                    })
                    issues[5] += 1;
                })
                xNextPage = response.headers.get('x-next-page')
            }
            setMemberIssues(issues);
            setIssueTotals(issues[5]);
        };
        if(isLoading){
          getCommits();
          getIssues();
          setLoading(false);
        }
    });

    if(isLoading) {
        return (<div className="App">Page currently loading...</div>);
    }

    if(isFailure) {
        return (<div className="App">Page failed to load</div>);
    }

    return (
        <Jumbotron id="aboutjumbotron">
            <h1> About The Project</h1>
            <br />
            <h3> What is Politicsin.review? </h3>
            <br />
            <p>
                We want to create a website that will display information about current 
                and past US congressional representatives, the types of laws they 
                introduced and/or voted on, and what industries/companies they receive funding from.
            </p>

            <h2 className="about-section-header">Team Members</h2>
                <CardDeck className="about-deck">
                    {members.map((member) => (
                        <MemberCard
                            first_name={member.first_name}
                            last_name={member.last_name}
                            job={member.job}
                            bio={member.bio}
                            commits={memberCommits[member.id]}
                            issues={memberIssues[member.id]}
                            tests={memberTests[member.id]}
                            pic={member.pic}
                            linkedin={member.linkedin}>
                        </MemberCard>
                    ))}
                </CardDeck>

            <div style={{ paddingBottom: '100px', paddingTop: '50px' }}>
                <Row xs={1} sm={3} className="stats-row">
                    <Col className="stats-col">
                        <h4>Total Commits</h4>
                        <h4>{commitTotals}</h4>
                    </Col>

                    <Col className="stats-col">
                        <h4>Total Issues</h4>
                        <h4>{issueTotals}</h4>
                    </Col>
                    <Col className="stats-col">
                        <h4>Total Tests</h4>
                        <h4>{testTotals}</h4>
                    </Col>
                </Row>
            </div>

            <div style={{ paddingBottom: '100px' }}>
                <h2 className='about-section-header'>Our Source APIs</h2>
                <br />
                <CardDeck className="about-deck">
                    {sources.map((tool) => (
                        <ToolCard
                            name={tool.name}
                            use={tool.use}
                            link={tool.link}
                            pic={tool.pic}>
                        </ToolCard>
                    ))}
                </CardDeck>
            </div>

            <div style={{ paddingBottom: '100px' }}>
                <h2 className='about-section-header'>Our Tools</h2>
                <br />
                <CardDeck className="about-deck">
                    {tools.map((tool) => (
                        <ToolCard
                            name={tool.name}
                            use={tool.use}
                            link={tool.link}
                            pic={tool.pic}>
                        </ToolCard>
                    ))}
                </CardDeck>
            </div>
            <h2 className='about-section-header'>Our Documentations</h2>
            <br />
            <div className="git-post" style={{ paddingBottom: '100px' }}>
                <CardDeck className="about-deck">
                    <Card className="about-card repo-card">
                        <a href="https://gitlab.com/ggujjula/cs373-idb/" target='_blank' rel="noreferrer">
                            <Card.Img className="about-img" src={gitlabPic} alt='gitlab'></Card.Img>
                        </a>
                        <br />
                        <Card.Text className="our-work-text">
                            GitLab Repo
                        </Card.Text>

                    </Card>
                    <Card className="about-card repo-card">
                        <a href="https://gitlab.com/ggujjula/cs373-idb/-/issues" target='_blank' rel="noreferrer">
                            <Card.Img className="about-img" src={gitlabPic} alt='gitlab'></Card.Img>
                        </a>
                        <br />
                        <Card.Text className="our-work-text">
                            GitLab Issues
                        </Card.Text>

                    </Card>
                    <Card className="about-card repo-card">
                        <a href="https://gitlab.com/ggujjula/cs373-idb/-/wikis/home" target='_blank' rel="noreferrer">
                            <Card.Img className="about-img" src={gitlabPic} alt='gitlab'></Card.Img>
                        </a>
                        <br />
                        <Card.Text className="our-work-text">
                            GitLab Wiki
                        </Card.Text>

                    </Card>
                    <Card className="about-card repo-card">
                        <a href="https://documenter.getpostman.com/view/16325742/Tzm5JxbR" target='_blank' rel="noreferrer">
                            <Card.Img className="about-img" src={postmanPic} alt='postman'></Card.Img>
                        </a>
                        <br />
                        <Card.Text className="our-work-text">
                            Our Postman API
                        </Card.Text>
                    </Card>
                    <Card className="about-card repo-card">
                        <a href={apiURL} target='_blank' rel="noreferrer">
                            <Card.Img className="about-img" src={testPic} alt='postman'></Card.Img>
                        </a>
                        <br />
                        <Card.Text className="our-work-text">
                            Test Our Backend!
                        </Card.Text>
                    </Card>
                    <Card className="about-card repo-card">
                        <a href="/critique" rel="noreferrer">
                            <Card.Img className="about-img" src={clipboardPic} alt='critique'></Card.Img>
                        </a>
                        <br />
                        <Card.Text className="our-work-text">
                            Project Self-Critique
                        </Card.Text>
                    </Card>
                    <Card className="about-card repo-card">
                        <a href="/consume/critique" rel="noreferrer">
                            <Card.Img className="about-img" src={clipboardPic} alt='critique'></Card.Img>
                        </a>
                        <br />
                        <Card.Text className="our-work-text">
                            Music-muse.me Project Critique
                        </Card.Text>
                    </Card>
                    <Card className="about-card repo-card">
                        <a href="https://speakerdeck.com/politicsinreview/idb-presentation" target="_blank" rel="noreferrer">
                            <Card.Img className="about-img" src={slideshowPic} alt='critique'></Card.Img>
                        </a>
                        <br />
                        <Card.Text className="our-work-text">
                            Speaker Deck Link
                        </Card.Text>
                    </Card>
                </CardDeck>
            </div>

        </Jumbotron>
    );
};

export default AboutPage;
