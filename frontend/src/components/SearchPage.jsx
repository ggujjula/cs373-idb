import React, {useEffect, useState} from "react";
import '../styles/index.css';
import '../styles/splashpage.css';
import Jumbotron from "react-bootstrap/Jumbotron";
import Button from "react-bootstrap/Button";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { useHistory, useParams } from "react-router-dom";
import axios from "axios";
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Form from 'react-bootstrap/Form';
import TabContainer from 'react-bootstrap/TabContainer';
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import BootstrapTable from "react-bootstrap-table-next";

const SearchPage = () => {
    
    const history = useHistory();
    const apiURL = process.env.REACT_APP_API_URL;
    const limit = 10;
    
    const {search} = useParams();
    const searchList = search.split(/\s+/);

    const [isLoadingBills, setLoadingBills] = useState(true);
    const [isFailureBills, setFailureBills] = useState(false);
    const [dataBills, setDataBills] = useState([]);

    const [isLoadingOrgs, setLoadingOrgs] = useState(true);
    const [isFailureOrgs, setFailureOrgs] = useState(false);
    const [dataOrgs, setDataOrgs] = useState([]);

    const [isLoadingPolis, setLoadingPolis] = useState(true);
    const [isFailurePolis, setFailurePolis] = useState(false);
    const [dataPolis, setDataPolis] = useState([]);

    const [numItemsBills, setNumItemsBills] = useState(0);
    const [pageBills, setPageBills] = useState(0);

    const [numItemsOrgs, setNumItemsOrgs] = useState(0);
    const [pageOrgs, setPageOrgs] = useState(0);

    const [numItemsPolis, setNumItemsPolis] = useState(0);
    const [pagePolis, setPagePolis] = useState(0);



    useEffect(() => {
        const appCall = async () => {
            setFailurePolis(false);
            setLoadingPolis(true);
            let searchString = "";
            if(searchList.length > 0) {
                searchString = "&q=" + searchList[0];
                for (let index = 1; index < searchList.length; index++) {
                    searchString += "+" + searchList[index];
                }
            }
            const nextUrl = "/politicians/?"
                + "limit=" + limit
                + "&offset=" + (pagePolis * limit)
                + "&sortby=id"
                + "&sortorder=asc"
                + searchString
            ;
            axios.get(nextUrl, {
                baseURL : apiURL,
                transformResponse: (r) => r,
                responseType: 'json',
                headers: {'Access-Control-Allow-Origin': '*'} })
            .then((response) => {
                if(response.status === 200 &&
                    response.data != null &&
                    response.data.results != null) {
                    console.log(response.data);
                    setDataPolis(response.data.results);
                    setNumItemsPolis(response.data.total);
                }
                else {
                    console.log("Response was not formatted correctly.");
                    setDataPolis([]);
                }
            })
            .catch((error) => {
                setFailurePolis(true);
            }).then( () => {
                setLoadingPolis(false);
            });
        };
        appCall();
    }, [apiURL, pagePolis]);

    useEffect(() => {
        const appCall = async () => {
            setFailureBills(false);
            setLoadingBills(true);
            let searchString = "";
            if(searchList.length > 0) {
                searchString = "&q=" + searchList[0];
                for (let index = 1; index < searchList.length; index++) {
                    searchString += "+" + searchList[index];
                }
            }
            const nextUrl = "/bills/?"
                + "limit=" + limit
                + "&offset=" + (pageBills * limit)
                + "&sortby=id"
                + "&sortorder=asc"
                + searchString
            ;
            axios.get(nextUrl, {
                baseURL : apiURL,
                transformResponse: (r) => r,
                responseType: 'json',
                headers: {'Access-Control-Allow-Origin': '*'} })
            .then((response) => {
                if(response.status === 200 &&
                    response.data != null &&
                    response.data.results != null) {
                    console.log(response.data);
                    setDataBills(response.data.results);
                    setNumItemsBills(response.data.total);
                }
                else {
                    console.log("Response was not formatted correctly.");
                    setDataBills([]);
                }
            })
            .catch((error) => {
                setFailureBills(true);
            }).then( () => {
                setLoadingBills(false);
            });
        };
        appCall();
    }, [apiURL, pageBills]);

    useEffect(() => {
        const appCall = async () => {
            setFailureOrgs(false);
            setLoadingOrgs(true);
            let searchString = "";
            if(searchList.length > 0) {
                searchString = "&q=" + searchList[0];
                for (let index = 1; index < searchList.length; index++) {
                    searchString += "+" + searchList[index];
                }
            }
            const nextUrl = "/organizations/?"
                + "limit=" + limit
                + "&offset=" + (pageOrgs * limit)
                + "&sortby=id"
                + "&sortorder=asc"
                + searchString
            ;
            axios.get(nextUrl, {
                baseURL : apiURL,
                transformResponse: (r) => r,
                responseType: 'json',
                headers: {'Access-Control-Allow-Origin': '*'} })
            .then((response) => {
                if(response.status === 200 &&
                    response.data != null &&
                    response.data.results != null) {
                    console.log(response.data);
                    setDataOrgs(response.data.results);
                    setNumItemsOrgs(response.data.total);
                }
                else {
                    console.log("Response was not formatted correctly.");
                    setDataOrgs([]);
                }
            })
            .catch((error) => {
                setFailureOrgs(true);
            }).then( () => {
                setLoadingOrgs(false);
            });
        };
        appCall();
    }, [apiURL, pageOrgs]);


    let displayPolis = (<h2> Error loading </h2>);

    function searchHighlight(cell) {
        let contents = String(cell);
        for(let keyword of searchList) {
            const expression = new RegExp(keyword, "ig")
            contents = contents.replaceAll(expression, "\\/" + keyword + "/\\");
        }
        contents = contents.split("\\")
        
        for(let index = 1; index < contents.length; index++) {
            if(contents[index].startsWith("/") && contents[index].endsWith("/")) {
                contents[index] = ( <mark> { contents[index].replaceAll("/","") } </mark> );
            }
        }
        return contents;
    }

    if(isLoadingPolis) {
        displayPolis = (<div className="App">Page currently loading...</div>);
    } else if(isFailurePolis) {
        displayPolis = (<div className="App">Page failed to load</div>);
    } else {
        const columns = [{
            dataField: "id",
            text: "Politician ID",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }, {
            dataField: "crp_id",
            text: "Open Secrets ID",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }, {
            dataField: "title",
            text: "Title",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }, {
            dataField: "first_name",
            text: "First Name",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }, {
            dataField: "last_name",
            text: "Last Name",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }, {
            dataField: "party",
            text: "Party",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
            }, {
            dataField: "state",
            text: "State",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }];
        const rowEvents = {
            onDoubleClick: (e, row, rowIndex) => {
                history.push("/politicians/" + row.id);
            }
        };
        displayPolis = (<BootstrapTable keyField='id' data={ dataPolis } columns={ columns } rowEvents={ rowEvents } rowStyle={{ backgroundColor: 'white' }}/>)
    }

    let displayOrgs = (<h2> Error loading </h2>);

    if(isLoadingOrgs) {
        displayOrgs = (<div className="App">Page currently loading...</div>);
    } else if(isFailureOrgs) {
        displayOrgs = (<div className="App">Page failed to load</div>);
    } else {
        const columns = [{
            dataField: "orgid",
            text: "Org ID",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }, {
            dataField: "orgname",
            text: "Org Name",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }, {
            dataField: "dems",
            text: "$ Donated to Democratic Candidates",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }, {
            dataField: "repubs",
            text: "$ Donated to Republican Candidates",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }, {
            dataField: "lobbying",
            text: "$ Spent Lobbying last cycle",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
            }, {
            dataField: "cycle",
            text: "Last Cycle Recorded",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }];
        const rowEvents = {
            onDoubleClick: (e, row, rowIndex) => {
                history.push("/organizations/" + row.orgid);
            }
        };
        displayOrgs = (<BootstrapTable keyField='id' data={ dataOrgs } columns={ columns } rowEvents={ rowEvents } rowStyle={{ backgroundColor: 'white' }}/>)
    }

    let displayBills = (<h2> Error loading </h2>);
    if(isLoadingBills) {
        displayBills = (<div className="App">Page currently loading...</div>);
    } else if(isFailureBills) {
        displayBills = (<div className="App">Page failed to load</div>);
    } else {
        const columns = [{
            dataField: "bill_id",
            text: "Bill ID",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }, {
            dataField: "sponsor_id",
            text: "Sponsor ID",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }, {
            dataField: "primary_subject",
            text: "Primary Subject",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }, {
            dataField: "title",
            text: "Bill Title",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }, {
            dataField: "latest_major_action",
            text: "Latest Action",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }, {
            dataField: "congressdotgov_url",
            text: "Congress.gov page",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                let contains = false;
                for(const keyword of searchList) {
                    if(!contains && String(cell).includes(keyword.toLowerCase())){
                        contains = true;
                    }
                }
                let linkText = "Visit Congress.gov Page"
                if(contains) {
                    linkText = ( <mark> { linkText } </mark> )
                }
                
                return ( <a href={cell} target="_blank" rel="noopener noreferrer">  { linkText } </a> )
            })
        }];
        const rowEvents = {
            onDoubleClick: (e, row, rowIndex) => {
                history.push("/bills/" + row.bill_id);
            }
        };
        displayBills = (<BootstrapTable keyField='id' data={ dataBills } columns={ columns } rowEvents={ rowEvents } rowStyle={{ backgroundColor: 'white' }}/>)
    }

    return (
        <div className="App" id="mainblock">
            <div>
                <Container fluid>
                <Row>
                    <Col>
                        <Jumbotron className="bg-light rounded-3" id="mainjumbotron">
                            <h1 id="title">Global Search Results</h1>
                            <h4 id="title2">Double click on a row for more information</h4>
                            <h4 id="title3">See tabs for results</h4>
                        </Jumbotron>
                    </Col>
                    { /*
                    <Col>
                        <Row md="auto">
                            <ButtonGroup>
                                <Button
                                    onClick={
                                        () => {
                                            setSearch(searchField.split(" "));
                                        }
                                    }
                                >
                                    Search
                                </Button>
                                <Form.Control  style={{height:"38px"}} value={ searchField } onChange={ (event) => { setSearchField(event.target.value) } }/>
                            <Button
                                    onClick={
                                        () => {
                                            setSearch([]);
                                            setSearchField("");
                                        }
                                    }
                                >
                                    Reset
                                </Button>
                            </ButtonGroup>
                        </Row>
                    </Col>
                    */}
                </Row>
                </Container>
            </div>
            <br></br>

            <TabContainer style={ {"background-color":"white"} }>
            <Tabs defaultActiveKey="polis" id="global-search-tabs" style={ {"background-color":"white"} }>
                <Tab eventKey="polis" title="Politician" style={ {"background-color":"white"} }>
                    <ButtonGroup>
                        <Button variant="primary" size="sm" onClick={ () => setPagePolis(0) } > First Page </Button>
                        <Button variant="primary" size="sm" onClick={ () => setPagePolis(Math.max(pagePolis - 1, 0)) } > Previous Page </Button>
                    </ButtonGroup>
                    
                    <ButtonGroup>
                    <Button variant="primary" size="sm"> Current Page: </Button>
                    <Form.Control  style={{width:"50px", height:"30px"}} value={ pagePolis } onChange={ (event) => { setPagePolis(Math.min(Math.floor(numItemsPolis / limit), event.target.value)) } }/>
                    </ButtonGroup>

                    <ButtonGroup>
                        <Button variant="primary" size="sm" onClick={ () => setPagePolis(pagePolis + 1) } > Next Page </Button>
                        <Button variant="primary" size="sm" onClick={ () => setPagePolis( Math.floor(numItemsPolis / limit) ) } > Last Page </Button>
                    </ButtonGroup>
                    { displayPolis } 
                </Tab>


                <Tab eventKey="bills" title="Bill" style={ {"background-color":"white"} }>
                    <ButtonGroup>
                        <Button variant="primary" size="sm" onClick={ () => setPageBills(0) } > First Page </Button>
                        <Button variant="primary" size="sm" onClick={ () => setPageBills(Math.max(pageBills - 1, 0)) } > Previous Page </Button>
                    </ButtonGroup>
                    
                    <ButtonGroup>
                    <Button variant="primary" size="sm"> Current Page: </Button>
                    <Form.Control  style={{width:"50px", height:"30px"}} value={ pageBills } onChange={ (event) => { setPageBills(Math.min(Math.floor(numItemsBills / limit), event.target.value)) } }/>
                    </ButtonGroup>

                    <ButtonGroup>
                        <Button variant="primary" size="sm" onClick={ () => setPageBills(pageBills + 1) } > Next Page </Button>
                        <Button variant="primary" size="sm" onClick={ () => setPageBills( Math.floor(numItemsBills / limit) ) } > Last Page </Button>
                    </ButtonGroup>
                    { displayBills }
                </Tab>


                <Tab eventKey="orgs" title="Organization" style={ {"background-color":"white"} }>
                <ButtonGroup>
                        <Button variant="primary" size="sm" onClick={ () => setPageOrgs(0) } > First Page </Button>
                        <Button variant="primary" size="sm" onClick={ () => setPageOrgs(Math.max(pageOrgs - 1, 0)) } > Previous Page </Button>
                    </ButtonGroup>
                    
                    <ButtonGroup>
                    <Button variant="primary" size="sm"> Current Page: </Button>
                    <Form.Control  style={{width:"50px", height:"30px"}} value={ pageOrgs } onChange={ (event) => { setPageOrgs(Math.min(Math.floor(numItemsOrgs / limit), event.target.value)) } }/>
                    </ButtonGroup>

                    <ButtonGroup>
                        <Button variant="primary" size="sm" onClick={ () => setPageOrgs(pageOrgs + 1) } > Next Page </Button>
                        <Button variant="primary" size="sm" onClick={ () => setPageOrgs( Math.floor(numItemsOrgs / limit) ) } > Last Page </Button>
                    </ButtonGroup>
                    { displayOrgs }
                </Tab>
            </Tabs>
            </TabContainer>
        </div>
    );
};

export default SearchPage;
