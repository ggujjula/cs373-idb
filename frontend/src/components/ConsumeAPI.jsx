import React from "react";
import PieChart from './PieChart.js'
import BubbleChart from './BubbleChart.js'
import BarChart from './BarChart.js'

import mostSongs from '../assets/Most_Songs.json'
import mostAlbums from '../assets/Albums_Most_Songs.json'
import longestSongs from '../assets/Longest_Songs.json'

/* page to display our visualizations */
const ConsumeAPI = () => {
    return (
        <div className="jumbotron flex-fill flex-fill flex-column align-content-center text-center">
            <h1 className="display-4 pageTitle">Music-muse.me Consume API</h1>
            
            <div style={{marginTop: '100px', family: "Sans-serif"}}>
                <h2>Top 10 Artists For The Most Number Of Songs</h2>
                <BubbleChart 
                    data={mostSongs}
                    />
            </div>
            
            <div style={{marginTop: '100px', family: "Sans-serif"}}>
                <h2>Top 10 Albums For The Most Number Of Songs</h2>
                <PieChart
                    data={mostAlbums}
                    innerRadius={0}
                    outerRadius={200}/>
            </div>

            <div style={{marginTop: '100px', family: "Sans-serif"}}>
                <h2>Top 10 Longest Songs On Spotify</h2>
                <BarChart
                    data={longestSongs}
                    xAttr="label"
                    yAttr="value"
                    xLabel="Song Name"
                    yLabel="Seconds"  
                    />
            </div>
        </div>
    );
};

export default ConsumeAPI;