import React, {useEffect, useState} from "react";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import Jumbotron from "react-bootstrap/Jumbotron";
import { useHistory } from "react-router-dom";
import axios from "axios";
import Button from 'react-bootstrap/Button';
import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Form from 'react-bootstrap/Form';



const validSubjects = [
    ["no_filter", "Primary Subject"],
    ["Congress", "Congress"],
    ["Native", "Native Americans"],
    ["Economics", "Economics and Public Finance"],
    ["Environmental", "Environmental Protection"],
    ["Energy", "Energy"],
    ["Armed", "Armed Forces and National Security"],
    ["Health", "Health"],
    ["Crime", "Crime and Law Enforcement"],
    ["Civil", "Civil Rights and Liberties, Minority Issues"],
    ["International", "International Affairs"],
    ["Government", "Government Operations and Politics"],
    ["Taxation", "Taxation"],
    ["Families", "Families"],
    ["Science", "Science, Technology, Communications"],
    ["Labor", "Labor and Employment"],
    ["Emergency", "Emergency Management"],
    ["Education", "Education"],
    ["Public", "Public Lands and Natural Resources"],
    ["Transportation", "Transportation and Public Works"],
    ["Social", "Social Welfare"],
    ["Commerce", "Commerce"],
    ["Law", "Law"],
    ["Housing", "Housing and Community Development"],
    ["Finance", "Finance and Financial Sector"],
    ["Immigration", "Immigration"],
    ["Foreign", "Foreign Trade and International Finance"],
    ["Arts", "Arts, Culture, Religion"],
    ["Agriculture", "Agriculture and Food"],
    ["Water", "Water Resources Development"]
]


const Bills = () => {
    const rowEvents = {
        onDoubleClick: (e, row, rowIndex) => {
            history.push("/bills/" + row.bill_id);
        }
    };
    const [isLoading, setLoading] = useState(true);
    const [isFailure, setFailure] = useState(false);
    const [data, setData] = useState();

    const [sortDropdown, setSortDropdown] = useState("Bill ID");
    const [sortBy, setSortBy] = useState("id");
    const [orderDropdown, setOrderDropdown] = useState("Ascending");
    const [sortOrder, setOrder] = useState("asc");

    const [subjectDropdown, setSubjectDropdown] = useState("Primary Subject");
    const [filterSubject, setSubject] = useState("no_filter");
    const [orgDropdown, setOrgDropdown] = useState("Org Data");
    const [filterOrgs, setOrgs] = useState("no_filter");
    
    const [searchField, setSearchField] = useState("");
    const [searchList, setSearch] = useState([]);

    const history = useHistory();
    const apiURL = process.env.REACT_APP_API_URL;
    let [page, setPage] = useState(0);
    const [numItems, setNumItems] = useState(0);
    const limit = 10;

    useEffect(() => {
        const appCall = async () => {
            setFailure(false);
            setLoading(true);
            let searchString = "";
            if(searchList.length > 0) {
                searchString = "&q=" + searchList[0];
                for (let index = 1; index < searchList.length; index++) {
                    searchString += "+" + searchList[index];
                }
            }
            const nextUrl = "/bills/?"
                + "limit=" + limit
                + "&offset=" + (page * limit)
                + "&sortby=" + sortBy
                + "&sortorder=" + sortOrder
                + "&subject=" + filterSubject
                + "&hasOrgs=" + filterOrgs
                + searchString
            ;
            axios.get(nextUrl, {
                baseURL : apiURL,
                transformResponse: (r) => r,
                responseType: 'json',
                headers: {'Access-Control-Allow-Origin': '*'} })
            .then((response) => {
                if(response.status === 200 &&
                    response.data != null &&
                    response.data.results != null) {
                    setData(response.data.results);
                    setNumItems(response.data.total);
                }
                else {
                    console.log("Response was not formatted correctly.");
                    setData([]);
                }
            })
            .catch((error) => {
                setFailure(true);
            }).then( () => {
                setLoading(false);
            });
        };
        appCall();
    }, [apiURL, page, sortBy, sortOrder, filterOrgs, filterSubject, searchList]);

    let display = (<h2> Error loading </h2>);

    if(isLoading) {
        display = (<div className="App">Page currently loading...</div>);
    } else if(isFailure) {
        display = (<div className="App">Page failed to load</div>);
    } else {
        function searchHighlight(cell) {
            let contents = String(cell);
            for(let keyword of searchList) {
                const expression = new RegExp(keyword, "ig")
                contents = contents.replaceAll(expression, "\\/" + keyword + "/\\");
            }
            contents = contents.split("\\")
            
            for(let index = 1; index < contents.length; index++) {
                if(contents[index].startsWith("/") && contents[index].endsWith("/")) {
                    contents[index] = ( <mark> { contents[index].replaceAll("/","") } </mark> );
                }
            }
            return contents;
        }

        const columns = [{
            dataField: "bill_id",
            text: "Bill ID",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }, {
            dataField: "sponsor_id",
            text: "Sponsor ID",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }, {
            dataField: "primary_subject",
            text: "Primary Subject",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }, {
            dataField: "title",
            text: "Bill Title",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }, {
            dataField: "latest_major_action",
            text: "Latest Action",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }, {
            dataField: "congressdotgov_url",
            text: "Congress.gov page",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                let contains = false;
                for(const keyword of searchList) {
                    if(!contains && String(cell).includes(keyword.toLowerCase())){
                        contains = true;
                    }
                }
                let linkText = "Visit Congress.gov Page"
                if(contains) {
                    linkText = ( <mark> { linkText } </mark> )
                }
                
                return ( <a href={cell} target="_blank" rel="noopener noreferrer">  { linkText } </a> )
            })
        }];
        display = (<BootstrapTable keyField='id' data={ data } columns={ columns } rowEvents={ rowEvents } rowStyle={{ backgroundColor: 'white' }}/>)
    }

    const subjectsDropdownItems = validSubjects.map((subject) => {
        return (
            <Dropdown.Item 
                onClick={ 
                    () => {
                        setPage(0);
                        
                        setSubjectDropdown(subject[1]);
                        setSubject(subject[0]);
                    }
                }
            >
                { subject[1] }
            </Dropdown.Item>
        )
    });

    return (
        <div className="App" id="mainblock">
            <div>
                <Container fluid>
                <Row>
                    <Col>
                        <Jumbotron className="bg-light rounded-3" id="mainjumbotron">
                            <h1 id="title">Bills Data Table</h1>
                            <h4 id="title3">Double click on a row for more information</h4>
                            <ButtonGroup>
                                <Button variant="primary" size="sm" onClick={ () => setPage(0) } > First Page </Button>
                                <Button variant="primary" size="sm" onClick={ () => setPage(Math.max(page - 1, 0)) } > Previous Page </Button>
                            </ButtonGroup>
                            
                            <ButtonGroup>
                            <Button variant="primary" size="sm"> Current Page: </Button>
                            <Form.Control  style={{width:"50px", height:"30px"}} value={ page } onChange={ (event) => { setPage(Math.min(Math.floor(numItems / limit), event.target.value)) } }/>
                            </ButtonGroup>

                            <ButtonGroup>
                                <Button variant="primary" size="sm" onClick={ () => setPage(page + 1) } > Next Page </Button>
                                <Button variant="primary" size="sm" onClick={ () => setPage( Math.floor(numItems / limit) ) } > Last Page </Button>
                            </ButtonGroup>
                        </Jumbotron>
                    </Col>
                    <Col>
                        <Row md="auto">
                        <ButtonGroup>
                            <DropdownButton as={ButtonGroup} title={ "Sort By: " + sortDropdown } id="sortby-dropdown">
                                <Dropdown.Item onClick={ () => {setPage(0); setSortDropdown("Bill ID"); setSortBy("id");} }>Bill ID</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setPage(0); setSortDropdown("Sponsor ID"); setSortBy("sponsor");} }>Sponsor ID</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setPage(0); setSortDropdown("Primary Subject"); setSortBy("subject");} }>Primary Subject</Dropdown.Item>
                            </DropdownButton>

                            <DropdownButton as={ButtonGroup} title={ orderDropdown } drop={ sortOrder==="asc"?"up":"down" } id="sortorder-dropdown">
                                <Dropdown.Item onClick={ () => {setPage(0); setOrderDropdown("Ascending"); setOrder("asc");} }>Ascending</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setPage(0); setOrderDropdown("Descending"); setOrder("desc");} }>Descending</Dropdown.Item>
                            </DropdownButton>
                        </ButtonGroup>
                        </Row>
                        <br></br>
                        <Row md="auto">
                        <ButtonGroup>
                            <Button
                                onClick={
                                    () => {
                                        setPage(0);

                                        setSubjectDropdown("Primary Subject");
                                        setSubject("no_filter");
                                        setOrgDropdown("Org Data");
                                        setOrgs("no_filter");
                                    }
                                }
                            >
                                Clear Filters
                            </Button>

                            <Dropdown as={ButtonGroup}>
                                <Dropdown.Toggle variant="primary" id="filter-subject-dropdown"> { subjectDropdown } </Dropdown.Toggle>
                                <Dropdown.Menu style={{height:"200px",overflowY:"auto"}}>
                                    { subjectsDropdownItems }
                                </Dropdown.Menu>
                            </Dropdown>

                            <DropdownButton as={ButtonGroup} title={ orgDropdown } id="filter-hasorgs-dropdown">
                                <Dropdown.Item onClick={ () => {setPage(0); setOrgDropdown("Org Data"); setOrgs("no_filter");} }>Org Data</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setPage(0); setOrgDropdown("hasOrgs"); setOrgs("True");} }>hasOrgs</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setPage(0); setOrgDropdown("noOrgs"); setOrgs("False");} }>noOrgs</Dropdown.Item>
                            </DropdownButton>
                        </ButtonGroup>
                        </Row>
                        <br></br>
                        <Row md="auto">
                            <ButtonGroup>
                                <Button
                                    onClick={
                                        () => {
                                            setSearch(searchField.split(" "));
                                        }
                                    }
                                >
                                    Search
                                </Button>
                                <Form.Control  style={{height:"38px"}} value={ searchField } onChange={ (event) => { setSearchField(event.target.value) } }/>
                            <Button
                                    onClick={
                                        () => {
                                            setSearch([]);
                                            setSearchField("");
                                        }
                                    }
                                >
                                    Reset
                                </Button>
                            </ButtonGroup>
                        </Row>
                    </Col>
                </Row>
                </Container>
            </div>
            <br></br>

            { display } 
        </div>
    );
};

export default Bills;
