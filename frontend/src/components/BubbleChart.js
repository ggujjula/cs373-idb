import React from 'react';
import BubbleChartOrig from "@weknow/react-bubble-chart-d3";

/* Bubble chart visualization used to present ours and our provider's data */
class BubbleChart extends React.Component {
    render() {
		return(
			<div className="BubbleChart">
				{/* used bubble chart React component from @weknowinc
				customizeable aspects are graph, height, legend
				percentage, and data */}
				<BubbleChartOrig
					graph={{
						zoom: 1,
						offsetX: 0.0,
						offsetY: 0.0,
					}}
					width={this.props.width}
					height={this.props.height}
					// optional value, pass false to disable the legend.
					showLegend={true}
					// number that represent the % of width that legend going to use.
					legendPercentage={20}
					legendFont={{
						family: "Sans-serif",
						size: 16,
						color: "#212529",
					}}
					valueFont={{
						family: "Sans-serif",
						size: 16,
						color: "#fff",
					}}
					labelFont={{
						family: "Sans-serif",
						size: 16,
						color: "#fff",
					}}
					data={this.props.data}
				/>
			</div>
		);
    }
}

export default BubbleChart;
