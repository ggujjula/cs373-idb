import secPic from '../images/sec.svg';
import propublicaPic from '../images/propublica.svg';
import opensecretsPic from '../images/opensecrets.svg';
import gitlabsPic from '../images/gitlab.svg';

export const sources = [

    {
        name: "Propublica Congress API",
        use: "Source for representative and bill data. The congress API uses a RESTful style. We use individual methods for permitted requests to get legislative data from the House of Representatives, the Senate and the Library of Congress.",
        link: "https://projects.propublica.org/api-docs/congress-api/",
        pic: propublicaPic,
    },
    {
        name: "data.sec.gov",
        use: "Source for company filing data. The SEC usinng RESTful data API delivering JSON-formatted data. This site doesn't require any authentication. We using Get request to optain metadata such as current name, former name and stock exchanges",
        link: "https://www.sec.gov/edgar/sec-api-documentation",
        pic: secPic,
    },
    {
        name: "OpenSecrets",
        use: "Source for company lobbying data. OpenSecrets provide RESTful API delivering XML or JSON formatted data. We uses 'canContrib' and 'candIndustry' calls to retrieves the data of top constributors to a candidate and the top industries to a candidate for indicated period",
        link: "https://www.opensecrets.org/open-data/api-documentation",
        pic: opensecretsPic,
    },
    {
        name: "Gitlab",
        use: "We use Gitlab API to track the number of issues and commits for each member.",
        link: "https://docs.gitlab.com/ee/api/index.html",
        pic: gitlabsPic,
    },
]
