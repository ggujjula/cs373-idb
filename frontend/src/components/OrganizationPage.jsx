import React, {useEffect, useState} from "react";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import Jumbotron from "react-bootstrap/Jumbotron";
import { useParams, useHistory } from "react-router-dom";
import axios from "axios";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import LiteYouTubeEmbed from 'react-lite-youtube-embed';
import 'react-lite-youtube-embed/dist/LiteYouTubeEmbed.css';
import Button from 'react-bootstrap/Button';


const columns = [{
    dataField: "orgid",
    text: "Org ID",
    headerStyle: { backgroundColor: 'white' }
}, {
    dataField: "orgname",
    text: "Org Name",
    headerStyle: { backgroundColor: 'white' }
}, {
    dataField: "dems",
    text: "$ Donated to Democratic Candidates",
    headerStyle: { backgroundColor: 'white' }
}, {
    dataField: "repubs",
    text: "$ Donated to Republican Candidates",
    headerStyle: { backgroundColor: 'white' }
}, {
    dataField: "lobbying",
    text: "$ Spent Lobbying last cycle",
    headerStyle: { backgroundColor: 'white' }
    }, {
    dataField: "cycle",
    text: "Last Cycle Recorded",
    headerStyle: { backgroundColor: 'white' }
}];

const OrganizationPage = () => {
    document.title = "Organizations";
    const {id} = useParams();
    const [isLoading, setLoading] = useState(true);
    const [isFailure, setFailure] = useState(false);
    const [youtubeVideos, setYoutubeVideos] = useState();
    const [data, setData] = useState();
    const history = useHistory();
    const apiURL = process.env.REACT_APP_API_URL;

    useEffect(() => {
        const appCall = async () => {
            setLoading(true);
            axios.get("/organizations/" + id, {
                baseURL : apiURL, 
                transformResponse: (r) => r,
                responseType: 'json',
                headers: {'Access-Control-Allow-Origin': '*'}})
            .then((response) => {
                setData(response.data);
            })
            .catch((error) => {
                setFailure(true);
            }).then( () => {
                setLoading(false);
            });
        };
        appCall();
    }, [apiURL, id]);

    useEffect(() => {
        if(data) {
            const { YoutubeDataAPI } = require("youtube-v3-api");
            const youtube_api = new YoutubeDataAPI(process.env.REACT_APP_YOUTUBE_API_KEY);
            setYoutubeVideos( (<div> <p> Content Loading </p> </div>) );
            const youtubeCall = async () => {
                youtube_api.searchAll(data.orgname, 2, {"type":"video"})
                .then((response) => {
                    const videos = response.items;
                    let result = videos.map((video) => {
                                let playlist = false
                                if(video.kind === "youtube#playlist") {
                                    playlist = true;
                                }
                                return (<LiteYouTubeEmbed
                                id={video.id.videoId} 
                                adNetwork={true} 
                                params=""  
                                playlist={playlist}
                                poster="hqdefault" 
                                title="YouTube Embed" 
                                cookie={false}
                                />)});
                    setYoutubeVideos(result);
                })
                .catch((error) => {
                    setYoutubeVideos( (<div> <h5> Data Not Available </h5> </div>) );
                });
            }
            youtubeCall();
        }
    }, [data]);

    if(isLoading) {
        return (<div className="App">Page currently loading...</div>);
    }

    if(isFailure) {
        return (<div className="App">Page failed to load</div>);
    }
    
    let bills = data.bills.map((bill) => {
        return (
            <Button size='sm' variant="primary" onClick={ () => {history.push("/bills/" + bill)} }> 
                { bill } 
            </Button>
        )
    });

    if(bills.length === 0) {
        bills = ( <h6> No indirectly supported bills in data </h6> )
    }

    let politicians = data.politicians.map((pol) => {
        return (
            <div>
                <Button size='sm' variant="primary" onClick={ () => {history.push("/politicians/" + pol.id)} }>
                { pol.name } 
                </Button>
            </div>
        )
    });

    if(politicians.length === 0) {
        politicians = ( <h6> No supported politicians in data </h6> )
    }

    return (
        <div className="App" id="mainblock">
            <div>
                <Jumbotron className="bg-light rounded-3" id="mainjumbotron">
                    <h1 id="title">Organization Instance</h1>
                </Jumbotron>
            </div>
            <br></br>

            <Container>
            <Row className="justify-content-md-center">
                <Col md="auto">
                    <Card style={{ width: '18rem' }}>
                        <Card.Header as="h2"> { data.orgname } </Card.Header>
                        <Card.Body>
                            <Card.Title as="h4"> { "ORG_ID: " + data.orgid } </Card.Title>
                            <h5> Amount Donated To: </h5>
                            <h5> Democrats: { " $" + data.dems } </h5>
                            <h5> Republicans: { " $" + data.repubs } </h5>
                        </Card.Body>
                    </Card>
                </Col>
                <Col md="auto">
                    <Card style={{ width: '18rem' }}>
                        <Card.Header as="h2"> { data.orgname } </Card.Header>
                        <Card.Body>
                            <Card.Title as="h4"> Total Spent Last Cycle: <br></br> { " $" + data.lobbying } </Card.Title>
                        </Card.Body>
                        <Card.Footer> <h5> Latest Cycle on Record: <br></br> { data.cycle } </h5></Card.Footer>
                    </Card>
                </Col>
                <Col md="auto">
                    <Card style={{ width: '18rem' }}>
                        <Card.Header as="h3"> Relevant Videos </Card.Header>
                        { youtubeVideos }
                    </Card>
                </Col>
                <Col md="auto">
                    <Card style={{ width: '18rem' }}>
                        <Card.Header as="h5"> Indirectly Supported Bills </Card.Header>
                        { bills }
                    </Card>
                    <Card style={{ width: '18rem' }}>
                        <Card.Header as="h5"> Politicians Donated To </Card.Header>
                        { politicians }
                    </Card>
                </Col>
            </Row>
            </Container>   

            <br></br><br></br>
            <BootstrapTable keyField='orgid' data={ [data] } columns={ columns } rowStyle={{ backgroundColor: 'white' }}/>   
        </div>
    );
};

export default OrganizationPage;
