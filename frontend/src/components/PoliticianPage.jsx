import React, {useEffect, useState} from "react";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import Jumbotron from "react-bootstrap/Jumbotron";
import { useParams, useHistory } from "react-router-dom";
import axios from "axios";
import { TwitterTimelineEmbed } from 'react-twitter-embed';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import LiteYouTubeEmbed from 'react-lite-youtube-embed';
import 'react-lite-youtube-embed/dist/LiteYouTubeEmbed.css';
import Button from 'react-bootstrap/Button';

const columns = [{
    dataField: "id",
    text: "Politician ID",
    headerStyle: { backgroundColor: 'white' }
}, {
    dataField: "crp_id",
    text: "Open Secrets ID",
    headerStyle: { backgroundColor: 'white' }
}, {
    dataField: "first_name",
    text: "First Name",
    headerStyle: { backgroundColor: 'white' }
}, {
    dataField: "last_name",
    text: "Last Name",
    headerStyle: { backgroundColor: 'white' }
}, {
    dataField: "party",
    text: "Party",
    headerStyle: { backgroundColor: 'white' }
    }, {
    dataField: "state",
    text: "State",
    headerStyle: { backgroundColor: 'white' }
}, {
    dataField: "title",
    text: "Title",
    headerStyle: { backgroundColor: 'white' }
}];

const PoliticianPage = () => {
    document.title = "Politician";
    const {id} = useParams();
    const apiURL = process.env.REACT_APP_API_URL;
    
    const [twitterFeed, setTwitterFeed] = useState( (<div> <p> Content Loading </p> </div>) );
    const [youtubeVideos, setYoutubeVideos] = useState();
    const [isLoading, setLoading] = useState(true);
    const [isFailure, setFailure] = useState(false);
    const [data, setData] = useState();
    const history = useHistory();

    ///*
    useEffect(() => {
        const appCall = async () => {
            setLoading(true);
            axios.get("/politicians/" + id, {
                baseURL : apiURL,
                transformResponse: (r) => r,
                responseType: 'json',
                headers: {'Access-Control-Allow-Origin': '*'}})
            .then((response) => {
                setData(response.data);
            })
            .catch((error) => {
                setFailure(true);
            }).then( () => {
                setLoading(false);
            });
        };
        appCall();
    }, [apiURL, id]);

    useEffect(() => {
        if(data) {
            const { YoutubeDataAPI } = require("youtube-v3-api");
            const youtube_api = new YoutubeDataAPI(process.env.REACT_APP_YOUTUBE_API_KEY);
            setYoutubeVideos( (<div> <p> Content Loading </p> </div>) );
            const youtubeCall = async (channelID) => {
                youtube_api.searchPlaylist(channelID, 3)
                .then((response) => {
                    const videos = response.items;
                    let result = videos.map((video) => {
                                let playlist = false
                                if(video.kind === "youtube#playlist") {
                                    playlist = true;
                                }
                                return (<LiteYouTubeEmbed
                                id={video.id} 
                                adNetwork={true} 
                                params=""  
                                playlist={playlist}
                                poster="hqdefault" 
                                title="YouTube Embed" 
                                cookie={false}
                                />)});
                    setYoutubeVideos(result);
                })
                .catch((error) => {
                    setYoutubeVideos( (<div> <h5> Data Not Available </h5> </div>) );
                });
            }

            const playlistCall = async () => {
                youtube_api.searchChannel(undefined, {forUsername:data.youtube, part:"contentDetails"})
                .then((response) => {
                    const channelID = response.items[0].id;
                    youtubeCall(channelID);
                })
                .catch((error) => {
                    setYoutubeVideos( (<div> <h5> Data Not Available </h5> </div>) );
                });
            }
            playlistCall();

            if(data.twitter) {
                setTwitterFeed( 
                    (<TwitterTimelineEmbed
                        sourceType="profile"
                        screenName={ data.twitter }
                        options={{height: 400}}
                        noFooter
                        noBorders
                        onLoad={ twitterFeedLoaded => {
                            if(!twitterFeedLoaded) {
                                setTwitterFeed( (<div> <h5> Data Not Available </h5> </div>) );
                            }
                        }}
                    />)
                );
            } else {
                setTwitterFeed( (<div> <h5> Data Not Available </h5> </div>) );
            }
        }
    }, [data]);

    /*
    const feedCall = async () => {
        let feed = await parser.parseURL(data.rss_url);
        feed.items.forEach(item => {
            console.log(item.title + ':' + item.link)
        });
    };
    feedCall();
    //*/ 


    if(isLoading) {
        return (<div className="App">Page currently loading...</div>);
    }

    if(isFailure) {
        return (<div className="App">Page failed to load</div>);
    }

    let sponsored_bills = data.sponsored_bills.map((bill) => {
        return (
            <Button size='sm' variant="primary" onClick={ () => {history.push("/bills/" + bill)} }> 
                { bill } 
            </Button>
        )
    });

    if(sponsored_bills.length === 0) {
        sponsored_bills = ( <h6> No sponsored bills in data </h6> )
    }

    let contributors = data.orgs.map((org) => {
        return (
            <div>
                <Button size='sm' variant="primary" onClick={ () => {history.push("/organizations/" + org.orgid)} }>
                { org.orgname } 
                </Button>
            </div>
        )
    });

    if(contributors.length === 0) {
        contributors = ( <h6> No Orgs in data </h6> )
    }


    return (
        <div className="App" id="mainblock">
            <div>
                <Jumbotron className="bg-light rounded-3" id="mainjumbotron">
                    <h1 id="title">Politician Instance</h1>
                </Jumbotron>
            </div>
            <br></br>


            <Container>
            <Row className="justify-content-md-center">
                <Col md="auto">
                    <Card style={{ width: '18rem' }}>
                        <Card.Header as="h2">{ data.first_name + " " + data.last_name }</Card.Header>
                        <Card.Body>
                            <Card.Title as="h4"> { data.title } </Card.Title>
                            <Card.Subtitle as="h5"> { data.party + " - " + data.state } </Card.Subtitle>
                            <Card.Text>
                                { "ID:" + data.id }
                                <br></br>
                                { "CRP_ID:" + data.crp_id }
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col md="auto">
                    <Card style={{ width: '18rem' }}>
                        <Card.Header as="h3"> Twitter Timeline</Card.Header>
                        { twitterFeed }
                    </Card>
                </Col>
                <Col md="auto">
                    <Card style={{ width: '18rem' }}>
                        <Card.Header as="h3"> Youtube Videos </Card.Header>
                        { youtubeVideos }
                    </Card>
                </Col>
                <Col md="auto">
                    <Card style={{ width: '18rem' }}>
                        <Card.Header as="h3"> Sponsored Bills </Card.Header>
                        { sponsored_bills }
                    </Card>
                    <Card style={{ width: '18rem' }}>
                        <Card.Header as="h3"> Top Contributors </Card.Header>
                        { contributors }
                    </Card>
                </Col>
            </Row>
            </Container>
        


            <br></br><br></br>
            <BootstrapTable keyField='id' data={ [data] } columns={ columns } rowStyle={{ backgroundColor: 'white' }}/>   
        </div>
    );
};

export default PoliticianPage;
