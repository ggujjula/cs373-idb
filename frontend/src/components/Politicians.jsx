import React, {useEffect, useState} from "react";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import Jumbotron from "react-bootstrap/Jumbotron";
import { useHistory } from "react-router-dom";
import axios from "axios";
import Button from 'react-bootstrap/Button';
import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Form from 'react-bootstrap/Form';
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';


const parties = ['Party' ,'D', 'R', 'ID']
const states = ['State','AK','AL','AR','AZ','CA','CO','CT','DC','DE','FL','GA','HI','IA','ID','IL','IN','KS','KY','LA','MA','MD','ME','MI','MN','MO','MP','MS','MT','NC','ND','NE','NH','NJ','NM','NV','NY','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VA','VT','WA','WI','WV','WY']
const titles = [
    ['no_filter', 'Title'],
    ['S1C', 'Senator, 1st Class'],
    ['S2C', 'Senator, 2nd Class'],
    ['S3C', 'Senator, 3rd Class'],
    ['rep', 'Representative'],
    ['del', 'Delegate']
]

const Politicians = () => {
    const rowEvents = {
        onDoubleClick: (e, row, rowIndex) => {
            history.push("/politicians/" + row.id);
        }
    };

    const [isLoading, setLoading] = useState(true);
    const [isFailure, setFailure] = useState(false);
    const [data, setData] = useState([]);

    const [sortDropdown, setSortDropdown] = useState("Politician ID");
    const [sortBy, setSortBy] = useState("id");
    const [orderDropdown, setOrderDropdown] = useState("Ascending");
    const [sortOrder, setOrder] = useState("asc");

    const [filterParty, setParty] = useState("Party");
    const [filterState, setState] = useState("State");
    const [titleDropdown, setTitleDropdown] = useState("Title");
    const [filterTitle, setTitle] = useState("no_filter");
    const [billDropdown, setBillDropdown] = useState("Bill Data");
    const [filterBills, setBills] = useState("no_filter");
    const [orgDropdown, setOrgDropdown] = useState("Org Data");
    const [filterOrgs, setOrgs] = useState("no_filter");
    
    const [searchField, setSearchField] = useState("");
    const [searchList, setSearch] = useState([]);

    const history = useHistory();
    const apiURL = process.env.REACT_APP_API_URL;
    const [numItems, setNumItems] = useState(0);
    const [page, setPage] = useState(0);
    const limit = 10;

    useEffect(() => {
        const appCall = async () => {
            setFailure(false);
            setLoading(true);
            let searchString = "";
            if(searchList.length > 0) {
                searchString = "&q=" + searchList[0];
                for (let index = 1; index < searchList.length; index++) {
                    searchString += "+" + searchList[index];
                }
            }
            const nextUrl = "/politicians/?"
                + "limit=" + limit
                + "&offset=" + (page * limit)
                + "&sortby=" + sortBy
                + "&sortorder=" + sortOrder
                + "&party=" + (filterParty==="Party"?"no_filter":filterParty)
                + "&state=" + (filterState==="State"?"no_filter":filterState)
                + "&title=" + filterTitle
                + "&hasBills=" + filterBills
                + "&hasOrgs=" + filterOrgs
                + searchString
            ;
            axios.get(nextUrl, {
                baseURL : apiURL,
                transformResponse: (r) => r,
                responseType: 'json',
                headers: {'Access-Control-Allow-Origin': '*'} })
            .then((response) => {
                if(response.status === 200 &&
                    response.data != null &&
                    response.data.results != null) {
                    console.log(response.data);
                    setData(response.data.results);
                    setNumItems(response.data.total);
                }
                else {
                    console.log("Response was not formatted correctly.");
                    setData([]);
                }
            })
            .catch((error) => {
                setFailure(true);
            }).then( () => {
                setLoading(false);
            });
        };
        appCall();
    }, [apiURL, page, sortBy, sortOrder, filterParty, filterState, filterTitle, filterOrgs, filterBills, searchList]);

    

    let display = (<h2> Error loading </h2>);

    if(isLoading) {
        display = (<div className="App">Page currently loading...</div>);
    } else if(isFailure) {
        display = (<div className="App">Page failed to load</div>);
    } else {
        function searchHighlight(cell) {
            let contents = String(cell);
            for(let keyword of searchList) {
                const expression = new RegExp(keyword, "ig")
                contents = contents.replaceAll(expression, "\\/" + keyword + "/\\");
            }
            contents = contents.split("\\")
            
            for(let index = 1; index < contents.length; index++) {
                if(contents[index].startsWith("/") && contents[index].endsWith("/")) {
                    contents[index] = ( <mark> { contents[index].replaceAll("/","") } </mark> );
                }
            }
            return contents;
        }
        
        const columns = [{
            dataField: "id",
            text: "Politician ID",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }, {
            dataField: "crp_id",
            text: "Open Secrets ID",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }, {
            dataField: "title",
            text: "Title",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }, {
            dataField: "first_name",
            text: "First Name",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }, {
            dataField: "last_name",
            text: "Last Name",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }, {
            dataField: "party",
            text: "Party",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
            }, {
            dataField: "state",
            text: "State",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }];
        display = (<BootstrapTable keyField='id' data={ data } columns={ columns } rowEvents={ rowEvents } rowStyle={{ backgroundColor: 'white' }}/>)
    }

    const partiesDropdownItems = parties.map((party) => {
        return (
            <Dropdown.Item 
                onClick={ 
                    () => {
                        setPage(0);

                        setParty(party);
                    }
                }
            >
                { party }
            </Dropdown.Item>
        )
    });
    const statesDropdownItems = states.map((state) => {
        return (
            <Dropdown.Item 
                onClick={ 
                    () => {
                        setPage(0);

                        setState(state);
                    }
                }
            >
                { state }
            </Dropdown.Item>
        )
    });
    const titlesDropdownItems = titles.map((title) => {
        return (
            <Dropdown.Item 
                onClick={ 
                    () => {
                        setPage(0);

                        setTitleDropdown(title[1]);
                        setTitle(title[0]);
                    }
                }
            >
                { title[1] }
            </Dropdown.Item>
        )
    });

    return (
        <div className="App" id="mainblock">
            <div>
                <Container fluid>
                <Row>
                    <Col>
                        <Jumbotron className="bg-light rounded-3" id="mainjumbotron">
                            <h1 id="title">Politicians Data Table</h1>
                            <h4 id="title3">Double click on a row for more information</h4>
                            
                            <ButtonGroup>
                                <Button variant="primary" size="sm" onClick={ () => setPage(0) } > First Page </Button>
                                <Button variant="primary" size="sm" onClick={ () => setPage(Math.max(page - 1, 0)) } > Previous Page </Button>
                            </ButtonGroup>
                            
                            <ButtonGroup>
                            <Button variant="primary" size="sm"> Current Page: </Button>
                            <Form.Control  style={{width:"50px", height:"30px"}} value={ page } onChange={ (event) => { setPage(Math.min(Math.floor(numItems / limit), event.target.value)) } }/>
                            </ButtonGroup>
                            
                            <ButtonGroup>
                                <Button variant="primary" size="sm" onClick={ () => setPage(page + 1) } > Next Page </Button>
                                <Button variant="primary" size="sm" onClick={ () => setPage( Math.floor(numItems / limit) ) } > Last Page </Button>
                            </ButtonGroup>
                        </Jumbotron>
                    </Col>

                    <Col>
                        <Row md="auto">
                        <ButtonGroup>
                            <DropdownButton as={ButtonGroup} title={ "Sort By: " + sortDropdown } id="sortby-dropdown">
                                <Dropdown.Item onClick={ () => {setPage(0); setSortDropdown("Politician ID"); setSortBy("id");} }>Politician ID</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setPage(0); setSortDropdown("Open Secrets ID"); setSortBy("crp_id");} }>Open Secrets ID</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setPage(0); setSortDropdown("First Name"); setSortBy("first_name");} }>First Name</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setPage(0); setSortDropdown("Last Name"); setSortBy("last_name");} }>Last Name</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setPage(0); setSortDropdown("Party"); setSortBy("party");} }>Party</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setPage(0); setSortDropdown("State"); setSortBy("state");} }>State</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setPage(0); setSortDropdown("Title"); setSortBy("title");} }>Title</Dropdown.Item>
                            </DropdownButton>

                            <DropdownButton as={ButtonGroup} title={ orderDropdown } drop={ sortOrder==="asc"?"up":"down" } id="sortorder-dropdown">
                                <Dropdown.Item onClick={ () => {setPage(0); setOrderDropdown("Ascending"); setOrder("asc");} }>Ascending</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setPage(0); setOrderDropdown("Descending"); setOrder("desc");} }>Descending</Dropdown.Item>
                            </DropdownButton>
                        </ButtonGroup>
                        </Row>
                        <br></br>
                        <Row md="auto">
                        <ButtonGroup>
                            <Button
                                onClick={
                                    () => {
                                        setPage(0);

                                        setParty("Party");
                                        setState("State");
                                        setTitleDropdown("Title");
                                        setTitle("no_filter");
                                        setBillDropdown("Bill Data");
                                        setBills("no_filter");
                                        setOrgDropdown("Org Data");
                                        setOrgs("no_filter");
                                    }
                                }
                            >
                                Clear Filters
                            </Button>

                            <DropdownButton as={ButtonGroup} title={ filterParty } id="filter-party-dropdown">
                                { partiesDropdownItems }
                            </DropdownButton>

                            <Dropdown as={ButtonGroup}>
                                <Dropdown.Toggle variant="primary" id="filter-state-dropdown"> { filterState } </Dropdown.Toggle>
                                <Dropdown.Menu style={{height:"200px",overflowY:"auto"}}>
                                    { statesDropdownItems }
                                </Dropdown.Menu>
                            </Dropdown>

                            <DropdownButton as={ButtonGroup} title={ titleDropdown } id="filter-title-dropdown">
                                { titlesDropdownItems }
                            </DropdownButton>

                            <DropdownButton as={ButtonGroup} title={ billDropdown } id="filter-hasbills-dropdown">
                                <Dropdown.Item onClick={ () => {setPage(0); setBillDropdown("Bill Data"); setBills("no_filter");} }>Bill Data</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setPage(0); setBillDropdown("hasBills"); setBills("True");} }>hasBills</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setPage(0); setBillDropdown("noBills"); setBills("False");} }>noBills</Dropdown.Item>
                            </DropdownButton>

                            <DropdownButton as={ButtonGroup} title={ orgDropdown } id="filter-hasorgs-dropdown">
                                <Dropdown.Item onClick={ () => {setPage(0); setOrgDropdown("Org Data"); setOrgs("no_filter");} }>Org Data</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setPage(0); setOrgDropdown("hasOrgs"); setOrgs("True");} }>hasOrgs</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setPage(0); setOrgDropdown("noOrgs"); setOrgs("False");} }>noOrgs</Dropdown.Item>
                            </DropdownButton>
                        </ButtonGroup>
                        </Row>
                        <br></br>
                        <Row md="auto">
                            <ButtonGroup>
                                <Button
                                    onClick={
                                        () => {
                                            setSearch(searchField.split(" "));
                                        }
                                    }
                                >
                                    Search
                                </Button>
                                <Form.Control  style={{height:"38px"}} value={ searchField } onChange={ (event) => { setSearchField(event.target.value) } }/>
                            <Button
                                    onClick={
                                        () => {
                                            setSearch([]);
                                            setSearchField("");
                                        }
                                    }
                                >
                                    Reset
                                </Button>
                            </ButtonGroup>
                        </Row>

                    </Col>
                </Row>
                </Container>
            </div>
            <br></br>

            { display } 
        </div>
    );
};

export default Politicians;
