/* importing team member pictures */
import GauthamPic from '../images/gautham.jpg';
import AustinPic from '../images/austin.jpg';
import SangPic from '../images/sang.jpg';
import AdithyaPic from '../images/adithya.png';
import RyanPic from '../images/ryan.jpg';

/* team member information for about page */
export const members = [
    {
        id: 0,
        first_name:   "Gautham",
        last_name:   "Gujjula",
        job:    "FullStack Engineer",
        bio:    "Gautham is a Senior at UT Austin. He working on full stack development for the site. His interests include: Networking, Compilers and Homelab",
        linkedin: '',
        pic:GauthamPic ,         
        
    },
    {
        id: 1,
        first_name:   "Austin",
        last_name:   "Dominquez",
        job:    "FullStack Engineer",
        bio:    "Austin is a Junior at UT Austin. He is originally from Houston, Texas. He works on full stack development for the site and is interested in quantum computing. Austin enjoys playing video games with friends, reading, and playing with his cat.",
        linkedin: '',
        pic: AustinPic,         
        
    },
    {
        id: 2,
        first_name:   "Sang",
        last_name:   "Pham",
        job:    "Frontend Engineer",
        bio:    "Sang is originally from Vietnam and is a Senior at UT Austin. He works on the frontend development for the wesite. He enjoy watching Youtube and playing volleyball on his free time.",
        linkedin: 'https://www.linkedin.com/in/sang-pham-2069ab177/',
        pic: SangPic,            
        
    },
    {
        id: 3,
        first_name:   "Adithya",
        last_name:   "Arunganesh",
        job:    "Frontend Engineer",
        bio:    "Adithya is originally from Plano, Texas and is a sophmore at UT Austin. He works on the API development and webapp for the site and is interested in data analytics. His hobbies include playing basketball, tennis, gaming and anime.",
        linkedin: '',
        pic:AdithyaPic ,             
        
    },
    {
        id: 4,
        first_name:   "Ryan",
        last_name:   "Travers",
        job:    "Frontend Engineer",
        bio:    "Ryan is a Senior at UT Austin. He is originally from Dallas, TX. He works on backend development for this site and is interested in game development and network security. Hobbies include: gaming, reading, cooking, volleyball, and DnD.",
        linkedin: '',
        pic:RyanPic ,           
        
    }
]
