import React, {useEffect, useState} from "react";
import Navbar from "react-bootstrap/Navbar"
import Nav from "react-bootstrap/Nav"
import Form from "react-bootstrap/Form"
import FormControl from "react-bootstrap/FormControl"
import Button from "react-bootstrap/Button"
import "../styles/navbar.css"
import { useHistory } from "react-router-dom";

const PIRNavbar = () => {
    let searchstr;

    const [submitSearch, setSubmitSearch] = useState(false);
    const [searchStr, setSearchStr] = useState("");
    const history = useHistory();

    useEffect(() => {
        if(submitSearch) {
            setSubmitSearch(false);
            history.push("/search/" + searchStr);
        }
    }, [submitSearch]);


    return (
        <Navbar bg="light" expand="lg" id="navbar">
            <Navbar.Brand href="/">Politics In Review</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
                <Nav.Link href="/politicians">Politicians</Nav.Link>
                <Nav.Link href="/bills">Bills</Nav.Link>
                <Nav.Link href="/organizations">Organizations</Nav.Link>
                <Nav.Link href="/consume/api">Consume API</Nav.Link>
                <Nav.Link href="/about">About</Nav.Link>
            </Nav>
            <Form className="d-flex" id="searchform">
                <Form.Control
                    type="search"
                    placeholder="Search"
                    className="mr-2"
                    aria-label="Search"
                    onChange={ (event) => { setSearchStr(event.target.value) } }
                />
                <Button variant="success"  onClick={ () => { setSubmitSearch(true) }} type="submit">Search</Button>
            </Form>
            </Navbar.Collapse>
        </Navbar>
    );
    
    
};

export default PIRNavbar;
