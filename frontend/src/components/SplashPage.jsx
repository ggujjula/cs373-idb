import React from 'react';
import '../styles/index.css';
import '../styles/splashpage.css';
import Jumbotron from "react-bootstrap/Jumbotron";
import Button from "react-bootstrap/Button";

class SplashPage extends React.Component{
    render () {
        return (
                    <div id="mainblock">
                        <Jumbotron className="bg-light rounded-3" id="mainjumbotron">
                            <h1 id="title">Politics In Review</h1>
                            <h4 id="description">See how politicians are funded and what bills they are involved with</h4>
                            <Button id="aboutbutton" href="/about" variant="primary">Learn More</Button>
                        </Jumbotron>
                    </div>
        )
    } 
}

export default SplashPage;
