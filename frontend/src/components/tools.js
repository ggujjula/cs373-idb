/* importing tools pictures */
import postmanPic from '../images/postman.svg';
import reactPic from '../images/react.svg';
import reactbootstrapPic from '../images/react-bootstrap.svg';
import gitlabPic from '../images/gitlab.svg';
import discordPic from '../images/discord.svg';

/* information for tools for about page */
export const tools = [

    {
        name: "Postman",
        use: "Testing both our own endpoints and the endpoints of the APIs we are using.",
        link: "https://www.postman.com/",
        pic: postmanPic,
    },
    {
        name: "React",
        use: "To code the front-end.",
        link: "https://reactjs.org/",
        pic: reactPic,
    },
    {
        name: "React-Bootstrap",
        use: "CSS framework for our website.",
        link: "https://react-bootstrap.github.io/",
        pic: reactbootstrapPic,
    },
    {
        name: "Gitlab",
        use: "Tracking our issues, user stories, and CI / CD pipelines.",
        link: "https://about.gitlab.com/",
        pic: gitlabPic,
    },
    {
        name: "Discord",
        use: "For communication and collaboration.",
        link: "https://discord.com/",
        pic: discordPic,
    },
]
