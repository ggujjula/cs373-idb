import React, {useEffect, useState} from "react";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import Jumbotron from "react-bootstrap/Jumbotron";
import { useParams, useHistory } from "react-router-dom";
import axios from "axios";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';


const columns = [{
    dataField: "bill_id",
    text: "Bill ID",
    headerStyle: { backgroundColor: 'white' }
}, {
    dataField: "sponsor_id",
    text: "Sponsor ID",
    headerStyle: { backgroundColor: 'white' }
}, {
    dataField: "primary_subject",
    text: "Primary Subject",
    headerStyle: { backgroundColor: 'white' }
}, {
    dataField: "title",
    text: "Bill Title",
    headerStyle: { backgroundColor: 'white' }
}, {
    dataField: "latest_major_action",
    text: "Latest Action",
    headerStyle: { backgroundColor: 'white' }
}, {
    dataField: "congressdotgov_url",
    text: "Congress.gov page",
    headerStyle: { backgroundColor: 'white' },
    formatter: ( (cell) => {
        return ( <a href={cell} target="_blank" rel="noopener noreferrer"> Visit Congress.gov Page </a> )
    })
}];

const BillPage = () => {
    document.title = "Organizations";
    const {id} = useParams();
    const [isLoading, setLoading] = useState(true);
    const [isFailure, setFailure] = useState(false);
    const [data, setData] = useState();
    const history = useHistory();
    const apiURL = process.env.REACT_APP_API_URL;

    useEffect(() => {
        const appCall = async () => {
            setLoading(true);
            axios.get("/bills/" + id, {
                baseURL : apiURL, 
                transformResponse: (r) => r,
                responseType: 'json',
                headers: {'Access-Control-Allow-Origin': '*'}})
            .then((response) => {
                setData(response.data);
            })
            .catch((error) => {
                setFailure(true);
            }).then( () => {
                setLoading(false);
            });
        };
        appCall();
    }, [apiURL, id]);

    if(isLoading) {
        return (<div className="App">Page currently loading...</div>);
    }

    if(isFailure) {
        return (<div className="App">Page failed to load</div>);
    }
    //const docs = [{ uri: data. }]

    let contributors = data.orgs.map((org) => {
        return (
            <div>
                <Button size='sm' variant="primary" onClick={ () => {history.push("/organizations/" + org.orgid)} }>
                { org.orgname } 
                </Button>
            </div>
        )
    });

    if(contributors.length === 0) {
        contributors = ( <h6> No Orgs in data </h6> )
    }

    return (
        <div className="App" id="mainblock">
            <div>
                <Jumbotron className="bg-light rounded-3" id="mainjumbotron">
                    <h1 id="title">Bill Instance</h1>
                </Jumbotron>
            </div>
            <br></br>

            <Container>
            <Row className="justify-content-md-center">
                <Col md="auto">
                    <Card style={{ width: '18rem' }}>
                        <Card.Body>
                            <Card.Title as="h4"> { "BILL_ID: " + data.bill_id } </Card.Title>
                            <Card.Subtitle as="h5"> { "SPONSOR_ID: " + data.sponsor_id } </Card.Subtitle>
                            <Card.Text>
                            <br></br> <h5> Primary Subject: </h5> { " " + data.primary_subject }
                            </Card.Text>
                        </Card.Body>
                        <Button size='sm' variant="primary" href={ data.congressdotgov_url } target="_blank" rel="noopener noreferrer">
                            Congress.gov Page
                        </Button>
                    </Card>
                </Col>
                <Col md="auto">
                    <Card style={{ width: '18rem' }}>
                        <Card.Header as="h2"> Bill Title </Card.Header>
                        <Card.Body>
                            <Card.Text>
                                { data.title }
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col md="auto">
                    <Card style={{ width: '18rem' }}>
                        <Card.Header as="h2"> Latest Action: </Card.Header>
                        <Card.Body>
                            { data.latest_major_action }
                        </Card.Body>
                    </Card>
                </Col>
                <Col md="auto">
                    <Card style={{ width: '18rem' }}>
                        <Card.Header as="h4"> Bill Sponsor </Card.Header>
                        <Button size='sm' variant="primary" onClick={ () => {history.push("/politicians/" + data.sponsor_id)} }>
                            Visit Sponsor Page
                        </Button>
                    </Card>
                    <Card style={{ width: '18rem' }}>
                        <Card.Header as="h4"> Sponsor's Contributors </Card.Header>
                        { contributors }
                    </Card>
                </Col>
            </Row>
            </Container>   

            
            <br></br><br></br>
            <BootstrapTable keyField='orgid' data={ [data] } columns={ columns } rowStyle={{ backgroundColor: 'white' }}/>   
        </div>
    );
};

export default BillPage;
