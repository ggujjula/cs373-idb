import React, { useState, useEffect } from 'react';
import { Jumbotron, Card, CardDeck, Row, Col, Container } from 'react-bootstrap';
import '../styles/critiquepage.css';

const AboutPage = () => {
    document.title = "Critique";

    return (
        <Jumbotron id="aboutjumbotron">
            <h1>Music-muse.me Project Critique</h1>
            <h2><a href="http://music-muse.me/">Music-muse.me</a></h2>
            <h3>What did they do well?</h3>
            
                <p className="critiquecontent">
                    They did a great job of getting data from all the API sources. The loading speed of all the pages
                    is extremely fast even with the huge amount of data. In addition, their sorting mechanism have
                    many options the data and they include a lot of useful columns especially image of album and song.
                    We also like how colofull and detail their spash page is.
                </p>

            <h3>What did we learn from their website?</h3>
                <p className="critiquecontent">
                    Their table looks very clean with the black and grey colors which we could apply to our pages. We also could
                    be make our spash page more dynamic.
                </p>
            <h3>What can they do better?</h3>
                <p className="critiquecontent">
                    For the pagination, we think they could have a first and last go to page options.
                    For the top songs page, it would be better if they have some explanation about how
                    the song is rated. Also it would be great if there's is genre filter.
                    We understand that there will be a search option coming so that could help with
                    all above issues.
                </p>
            <h3>What puzzles us about their website?</h3>
                <p className="critiquecontent">
                    We're little confuse about how the length of song on Top Songs page work because The youtube
                    video that each one provide doesn't match the length on the page. Other than that, everything is great!
                </p>
        </Jumbotron>
    );
};

export default AboutPage;
