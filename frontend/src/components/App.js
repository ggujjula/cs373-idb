import {
    BrowserRouter as Router,
    Route,
    Switch
} from "react-router-dom";
import PIRNavbar from "./PIRNavbar.jsx";
import SplashPage from "./SplashPage.jsx";
import Bills from "./Bills.jsx"
import Organizations from "./Organizations.jsx"
import Politicians from "./Politicians.jsx"
import BillPage from "./BillPage.jsx"
import OrganizationPage from "./OrganizationPage.jsx"
import PoliticianPage from "./PoliticianPage.jsx"
import AboutPage from "./AboutPage.jsx"
import SearchPage from "./SearchPage.jsx"
import CritiquePage from "./CritiquePage.jsx"
import ConsumeCritique from "./ConsumeCritique.jsx"
import ConsumeAPI from "./ConsumeAPI.jsx"

function App() {
  return (
    <div id="jsxcontainer" className="d-flex flex-column">
        <Router>
            <PIRNavbar />
            <Switch>
                <Route exact path="/">
                    <SplashPage />
                </Route>
                <Route exact path="/bills">
                    <Bills />
                </Route>
                <Route exact path="/politicians">
                    <Politicians />
                </Route>
                <Route exact path="/organizations">
                    <Organizations />
                </Route>
                <Route path="/bills/:id"
                    render={(props) => <BillPage {...props} />} 
                 />
                <Route path="/politicians/:id"
                    render={(props) => <PoliticianPage {...props} />} 
                 />
                <Route path="/organizations/:id"
                    render={(props) => <OrganizationPage {...props} />} 
                 />
                <Route path="/about">
                    <AboutPage />
                </Route>
                <Route path="/search/:search"
                    render={(props) => <SearchPage {...props} />} 
                 />
                <Route path="/critique">
                    <CritiquePage />
                </Route>
                <Route path="/consume/critique">
                    <ConsumeCritique />
                </Route>
                <Route exact path="/consume/api">
                    <ConsumeAPI/>
                </Route>
            </Switch>
        </Router>
    </div>
  );
}

export default App;
