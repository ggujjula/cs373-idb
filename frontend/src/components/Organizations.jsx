import React, {useEffect, useState} from "react";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import Jumbotron from "react-bootstrap/Jumbotron";
import { useHistory } from "react-router-dom";
import axios from "axios";
import Button from 'react-bootstrap/Button';
import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Form from 'react-bootstrap/Form';



const Organizations = () => {
    const rowEvents = {
        onDoubleClick: (e, row, rowIndex) => {
            history.push("/organizations/" + row.orgid);
        }
    };
    const [isLoading, setLoading] = useState(true);
    const [isFailure, setFailure] = useState(false);
    const [data, setData] = useState([]);

    const [sortDropdown, setSortDropdown] = useState("Org ID");
    const [sortBy, setSortBy] = useState("id");
    const [orderDropdown, setOrderDropdown] = useState("Ascending");
    const [sortOrder, setOrder] = useState("asc");

    const [demsDropdown, setDemsDropdown] = useState("$ to Democrats");
    const [filterDems, setDems] = useState("0");
    const [repubsDropdown, setRepubsDropdown] = useState("$ to Republicans");
    const [filterRepubs, setRepubs] = useState("0");
    const [lobbyingDropdown, setLobbyingDropdown] = useState("Total Spent Lobbying");
    const [filterLobbying, setLobbying] = useState("0");
    const [updateNumbers, setUpdateNumbers] = useState(false);

    const [billDropdown, setBillDropdown] = useState("Bill Data");
    const [filterBills, setBills] = useState("no_filter");
    const [poliDropdown, setPoliDropdown] = useState("Politician Data");
    const [filterPolis, setPolis] = useState("no_filter");
    
    const [searchField, setSearchField] = useState("");
    const [searchList, setSearch] = useState([]);

    const history = useHistory();
    const apiURL = process.env.REACT_APP_API_URL;
    const [page, setPage] = useState(0);
    const [numItems, setNumItems] = useState(0);
    const limit = 10;

    useEffect(() => {
        const appCall = async () => {
            setFailure(false);
            setLoading(true);
            let searchString = "";
            if(searchList.length > 0) {
                searchString = "&q=" + searchList[0];
                for (let index = 1; index < searchList.length; index++) {
                    searchString += "+" + searchList[index];
                }
            }
            const lobbyStr = "&" + lobbyingDropdown + "=" + filterLobbying;
            const demStr = "&" + demsDropdown + "=" + filterDems;
            const repubStr = "&" + repubsDropdown + "=" + filterRepubs;
            const nextUrl = "/organizations/?"
                + "limit=" + limit
                + "&offset=" + (page * limit)
                + "&sortby=" + sortBy
                + "&sortorder=" + sortOrder
                + (lobbyingDropdown==="Total Spent Lobbying"?"":lobbyStr)
                + (demsDropdown==="$ to Democrats"?"":demStr)
                + (repubsDropdown==="$ to Republicans"?"":repubStr)
                + "&hasBills=" + filterBills
                + "&hasPoliticians=" + filterPolis
                + searchString
            ;
            axios.get(nextUrl, {
                baseURL : apiURL,
                transformResponse: (r) => r,
                responseType: 'json',
                headers: {'Access-Control-Allow-Origin': '*'} })
            .then((response) => {
                if(response.status === 200 &&
                    response.data != null &&
                    response.data.results != null) {
                    setData(response.data.results);
                    setNumItems(response.data.total);
                }
                else {
                    console.log("Response was not formatted correctly.");
                    setData([]);
                }
            })
            .catch((error) => {
                setFailure(true);
            }).then( () => {
                setLoading(false);
            });
        };
        appCall();
    }, [apiURL, page, sortBy, sortOrder, filterBills, filterPolis, updateNumbers, searchList]);
    
    let display = (<h2> Error loading </h2>);

    if(isLoading) {
        display = (<div className="App">Page currently loading...</div>);
    } else if(isFailure) {
        display = (<div className="App">Page failed to load</div>);
    } else {
        function searchHighlight(cell) {
            let contents = String(cell);
            for(let keyword of searchList) {
                const expression = new RegExp(keyword, "ig")
                contents = contents.replaceAll(expression, "\\/" + keyword + "/\\");
            }
            contents = contents.split("\\")
            
            for(let index = 1; index < contents.length; index++) {
                if(contents[index].startsWith("/") && contents[index].endsWith("/")) {
                    contents[index] = ( <mark> { contents[index].replaceAll("/","") } </mark> );
                }
            }
            return contents;
        }

        const columns = [{
            dataField: "orgid",
            text: "Org ID",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }, {
            dataField: "orgname",
            text: "Org Name",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }, {
            dataField: "dems",
            text: "$ Donated to Democratic Candidates",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }, {
            dataField: "repubs",
            text: "$ Donated to Republican Candidates",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }, {
            dataField: "lobbying",
            text: "$ Spent Lobbying last cycle",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
            }, {
            dataField: "cycle",
            text: "Last Cycle Recorded",
            headerStyle: { backgroundColor: 'white' },
            formatter: ( (cell) => {
                const result = searchHighlight(cell);
                return ( <div> { result } </div> );
            })
        }];

        display = (<BootstrapTable keyField='id' data={ data } columns={ columns } rowEvents={ rowEvents } rowStyle={{ backgroundColor: 'white' }}/>)
    }

    return (
        <div className="App" id="mainblock">
            <div>
                <Container fluid>
                <Row>
                    <Col>
                        <Jumbotron className="bg-light rounded-3" id="mainjumbotron">
                            <h1 id="title">Organizations Data Table</h1>
                            <h4 id="title3">Double click on a row for more information</h4>
                            <ButtonGroup>
                                <Button variant="primary" size="sm" onClick={ () => setPage(0) } > First Page </Button>
                                <Button variant="primary" size="sm" onClick={ () => setPage(Math.max(page - 1, 0)) } > Previous Page </Button>
                            </ButtonGroup>
                            
                            <ButtonGroup>
                            <Button variant="primary" size="sm"> Current Page: </Button>
                            <Form.Control  style={{width:"50px", height:"30px"}} value={ page } onChange={ (event) => { setPage(Math.min(Math.floor(numItems / limit), event.target.value)) } }/>
                            </ButtonGroup>
                            
                            <ButtonGroup>
                                <Button variant="primary" size="sm" onClick={ () => setPage(page + 1) } > Next Page </Button>
                                <Button variant="primary" size="sm" onClick={ () => setPage( Math.floor(numItems / limit) ) } > Last Page </Button>
                            </ButtonGroup>
                        </Jumbotron>
                    </Col>

                    <Col>
                        <Row md="auto">
                        <ButtonGroup>
                            <DropdownButton as={ButtonGroup} title={ "Sort By: " + sortDropdown } id="sortby-dropdown">
                                <Dropdown.Item onClick={ () => {setPage(0); setSortDropdown("Org ID"); setSortBy("id");} }>Org ID</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setPage(0); setSortDropdown("Org Name"); setSortBy("name");} }>Org Name</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setPage(0); setSortDropdown("$ to Democrats"); setSortBy("dems");} }>$ to Democrats</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setPage(0); setSortDropdown("$ to Republicans"); setSortBy("repubs");} }>$ to Republicans</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setPage(0); setSortDropdown("Total Spent Lobbying"); setSortBy("lobbying");} }>Lobbying</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setPage(0); setSortDropdown("Latest Cycle"); setSortBy("cycle");} }>Cycle</Dropdown.Item>
                            </DropdownButton>

                            <DropdownButton as={ButtonGroup} title={ orderDropdown } drop={ sortOrder==="asc"?"up":"down" } id="sortorder-dropdown">
                                <Dropdown.Item onClick={ () => {setPage(0); setOrderDropdown("Ascending"); setOrder("asc");} }>Ascending</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setPage(0); setOrderDropdown("Descending"); setOrder("desc");} }>Descending</Dropdown.Item>
                            </DropdownButton>
                        </ButtonGroup>
                        </Row>
                        <br></br>
                        <Row>
                        <ButtonGroup>
                            <Button
                                style={{height:"38px", width:"350px"}}
                                onClick={
                                    () => {
                                        setPage(0);

                                        setUpdateNumbers(!updateNumbers);
                                    }
                                }
                            >
                                Update # Filters
                            </Button>
                            <Button
                                style={{height:"38px", width:"300px"}}
                                onClick={
                                    () => {
                                        setPage(0);

                                        setDemsDropdown("$ to Democrats");
                                        setDems("0");
                                        setRepubsDropdown("$ to Republicans");
                                        setRepubs("0");
                                        setLobbyingDropdown("Total Spent Lobbying");
                                        setLobbying("0");
                                        setBillDropdown("Bill Data");
                                        setBills("no_filter");
                                        setPoliDropdown("Politician Data");
                                        setPolis("no_filter");
                                    }
                                }
                            >
                                Clear Filters
                            </Button>

                            <DropdownButton style={{height:"38px"}} as={ButtonGroup} title={ billDropdown } id="filter-hasbills-dropdown">
                                <Dropdown.Item onClick={ () => {setPage(0); setBillDropdown("Bill Data"); setBills("no_filter");} }>Bill Data</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setPage(0); setBillDropdown("hasBills"); setBills("True");} }>hasBills</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setPage(0); setBillDropdown("noBills"); setBills("False");} }>noBills</Dropdown.Item>
                            </DropdownButton>

                            <DropdownButton style={{height:"38px"}} as={ButtonGroup} title={ poliDropdown } id="filter-haspolis-dropdown">
                                <Dropdown.Item onClick={ () => {setPage(0); setPoliDropdown("Politician Data"); setPolis("no_filter");} }>Politician Data</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setPage(0); setPoliDropdown("hasPoliticians"); setPolis("True");} }>hasPoliticians</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setPage(0); setPoliDropdown("noPoliticians"); setPolis("False");} }>noPoliticans</Dropdown.Item>
                            </DropdownButton>

                            <DropdownButton style={{height:"38px"}} as={ButtonGroup} title={ lobbyingDropdown } id="filter-lobbying-dropdown">
                                <Dropdown.Item onClick={ () => {setLobbyingDropdown("Total Spent Lobbying"); setLobbying("0");} }>Total Spent Lobbying</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setLobbyingDropdown("maxLobby");} }>maxLobby</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setLobbyingDropdown("minLobby");} }>minLobby</Dropdown.Item>
                            </DropdownButton>
                            <Form.Control  style={{height:"38px"}} value={ filterLobbying } onChange={ (event) => { setLobbying("" + event.target.value) } }/>
                        </ButtonGroup>
                        </Row>
                        <Row>
                        <ButtonGroup>
                            <DropdownButton as={ButtonGroup} title={ demsDropdown } id="filter-dems-dropdown">
                                <Dropdown.Item onClick={ () => {setDemsDropdown("$ to Democrats"); setDems("0");} }>$ to Democrats</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setDemsDropdown("maxDems");} }>maxDems</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setDemsDropdown("minDems");} }>minDems</Dropdown.Item>
                            </DropdownButton>
                            <Form.Control  style={{height:"38px"}} value={ filterDems } onChange={ (event) => { setDems("" + event.target.value) } }/>

                            <DropdownButton as={ButtonGroup} title={ repubsDropdown } id="filter-repubs-dropdown">
                                <Dropdown.Item onClick={ () => {setRepubsDropdown("$ to Republicans"); setRepubs("0");} }>$ to Republicans</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setRepubsDropdown("maxRepubs");} }>maxRepubs</Dropdown.Item>
                                <Dropdown.Item onClick={ () => {setRepubsDropdown("minRepubs");} }>minRepubs</Dropdown.Item>
                            </DropdownButton>
                            <Form.Control  style={{height:"38px"}} value={ filterRepubs } onChange={ (event) => { setRepubs("" + event.target.value) } }/>
                        </ButtonGroup>
                        </Row>
                        <br></br>
                        <Row md="auto">
                            <ButtonGroup>
                                <Button
                                    onClick={
                                        () => {
                                            setSearch(searchField.split(" "));
                                        }
                                    }
                                >
                                    Search
                                </Button>
                                <Form.Control  style={{height:"38px"}} value={ searchField } onChange={ (event) => { setSearchField(event.target.value) } }/>
                            <Button
                                    onClick={
                                        () => {
                                            setSearch([]);
                                            setSearchField("");
                                        }
                                    }
                                >
                                    Reset
                                </Button>
                            </ButtonGroup>
                        </Row>
                    </Col>
                </Row>
                </Container>
            </div>
            <br></br>

            { display }
        </div>
    );
};

export default Organizations;
