import React, { useState, useEffect } from 'react';
import { Jumbotron, Card, CardDeck, Row, Col, Container } from 'react-bootstrap';
import '../styles/critiquepage.css';

const AboutPage = () => {
    document.title = "Critique";

    return (
        <Jumbotron id="aboutjumbotron">
            <h1>Project Self-Critique</h1>
            <br />
            <h3>What did we do well?</h3>
            <h4 className="critiquesubsection">Logistics</h4>
                <p className="critiquecontent">
                    We were able to effectively split the project into different areas (DB, webapp, api, 
                    frontend, and devops) and delegate work between group members. This was done with
                    two tools: GitLab and Discord. We were able to separate the different areas of work
                    by using various GitLab issue labels and creating a separate Discord channel for
                    each area. Delegation worked by assigning issues to a team member, maintaining a
                    CODEOWNERS file, and triaging bugs via daily sync-up meetings.
                </p>
            <h4 className="critiquesubsection">Sources</h4>
                <p className="critiquecontent">
                    The RESTful sources for our data are from reputable organizations, such as Propublica,
                    OpenSecrets, and the US SEC.
                </p>
            <h4 className="critiquesubsection">Application Design</h4>
                <p className="critiquecontent">
                    We believe the layout of the website
                    and design of our API were both well-thought out. Both are fairly simple and fast,
                    while providing the information requested in an easy-to-parse way.
                </p>
            <h4 className="critiquesubsection">Testing and Deployment</h4>
                <p className="critiquecontent">
                    The testing and deployment process for our project is very good. The entire application
                    can be brought up locally with Docker Compose; this makes development easy and
                    consistent across devices. Also, the entire testing and deployment process was
                    automated with GitLab CI pipelines. Both the backend and frontend are first tested.
                    Then, production-ready containers are built and pushed to Google Container Registry,
                    where they are then deployed to Google Cloud Run.
                </p>
            <br />
            <h3>What did we learn?</h3>
                <p className="critiquecontent">
                    During the development of this project, we learned a great deal as a team. At the
                    start of the semester, we collectively had little experience with almost all of the
                    software stack that was required. Through creating this application, all of us have
                    learned the basics of databases, APIs, website development, and the overall development
                    and deployment process for a public-facing product. Specifically, we have learned a
                    great deal about PostgreSQL, Flask, React, GCP, git and GitLab, Docker, and Postman.
                </p>
            <br />
            <h3>What can we do better?</h3>
            <h4 className="critiquesubsection">Instance Pages</h4>
                <p className="critiquecontent">
                    The instance pages (like /politicians/B001230) have a few elements that may not be
                    optimal. For example, the background image of the Capitol clashes with the modal-based
                    design in the foreground. Additionally, a table containing a redundant summary of the
                    instance data is at the bottom of each page. Both of these points make the page less
                    readable; simplifying these pages may be a solution.
                </p>
            <h4 className="critiquesubsection">List Pages</h4>
                <p className="critiquecontent">
                    The list pages have a small
                    issue where the page goes blank between page turns. In terms of data, we could do better
                    with linking together the models we display. Some of the instances we show do not have
                    links to instances of other models, which makes the user experience suboptimal and the
                    website not as meaningful. A more refined audit of our data can fix this issue.
                </p>

            <h4 className="critiquesubsection">Security</h4>
                <p className="critiquecontent">
                    Security is another area where we could definitely do much better. In the crunch to get
                    the application ready in time for submission, we have not implemented good security
                    practices, and as a result we have exposed API keys and DB passwords in our builds.
                    Hopefully these issues are fixed by the time you are reading this, but at the time of
                    writing it is still something we need to improve on.
                </p>

            <h4 className="critiquesubsection">CI Pipeline</h4>
                <p className="critiquecontent">
                    The CI pipeline we have set up for our project is good in that it is fully functional.
                    However, it is fairly slow because it does not utilize caches when running Docker builds.
                    This was intentionally disabled as a workaround for an issue where Docker was using the
                    cache from previous builds incorrectly. So, the pipeline could be significantly improved
                    by finding the source of this issue and re-enabling caches correctly.
                </p>
                        
            <h4 className="critiquesubsection">Repo</h4>
                <p className="critiquecontent">
                    Using the GitLab repo was fairly straightforward. The only hiccup was the approval process
                    for PRs into main, which slowed down development significantly. We could improve our development
                    process by improving the approval process or finding an alternative.
                </p>

            <h4 className="critiquesubsection">React</h4>
                <p className="critiquecontent">
                    React is an awesome framework, and it was a great for us for this application. However, since
                    we were inexperienced, we did not stick with a consistent style for the entire frontend. As a
                    result, some of our components are class-based, while others are functional. While this is not
                    a functional issue, it may cause maintenance issues down the road. Refactoring all components
                    to be functional would fix this issue.
                </p>
            <br />
            <h3>What puzzles us?</h3>
            <h4 className="critiquesubsection">React</h4>
                <p className="critiquecontent">
                    As mentioned previously, our inexperience with React led to a bit of confusion with class-based
                    and functional components. More specifically, the use of React Hooks within functional components
                    was especially confusing. For example, useEffect() combines many portions of the components life
                    cycle that used to be isolated in different functions, which made it difficult understand. We
                    definitely need some more time to fully understand React Hooks. 
                </p>
            <h4 className="critiquesubsection">Databases</h4>
                <p className="critiquecontent">
                    Currently, the application repopulates the database provided in DB_STRING with data from cached
                    json files. This makes it easy to migrate the backend service and spin it up in a testing
                    environment, but is an expensive operation that does not scale well. The alternative would be to
                    configure the database separately and have the webapp only read from it, but this looks like it
                    would make maintaining the DB cumbersome. We need to do some more research and testing on how
                    to sustainably populate the DB with data. 
                </p>
        </Jumbotron>
    );
};

export default AboutPage;
