.PHONY: IDB1.log
.PHONY: IDB2.log
.PHONY: IDB3.log

ifeq ($(shell uname -n), swevm)          # My SWE VM (PVE)
    PYTHON   := python3
    PIP      := pip3
    PYLINT   := pylint
    COVERAGE := python3 -m coverage
    PYDOC    := pydoc3
    AUTOPEP8 := autopep8
else ifeq ($(shell uname), Darwin)          # Apple
    PYTHON   := python3
    PIP      := pip3
    PYLINT   := pylint
    COVERAGE := coverage
    PYDOC    := pydoc3
    AUTOPEP8 := autopep8
else ifeq ($(shell uname -p), unknown) # Windows
    PYTHON   := python                 # on my machine it's python
    PIP      := pip3
    PYLINT   := pylint
    COVERAGE := coverage
    PYDOC    := python -m pydoc        # on my machine it's pydoc
    AUTOPEP8 := autopep8
else                                   # UTCS
    PYTHON   := python3
    PIP      := pip3
    PYLINT   := pylint3
    COVERAGE := coverage
    PYDOC    := pydoc3
    AUTOPEP8 := autopep8
endif


IDB1.log:
	git log > IDB1.log
IDB2.log:
	git log > IDB2.log
IDB3.log:
	git log > IDB3.log

models.html: webapp/models.py
	$(PYDOC) -w webapp/models.py
