# Politics In Review

Summer 2021 SWE project

Link to Site: https://politicsin.review

## Building

### Development

To create a development environment, run:

```
sudo docker-compose build
sudo docker-compose up
```

Change the COMPOSE_HOST variable in the .env file if needed.

### Production
#### Frontend

To create a production build, run:

`cd frontend`

`npm install --force`

`npm run build`

### Backend

To create a production build, run:

`cd webapp`

`sudo docker build -t <image tag> .`

## Links

Issue Tracker: https://gitlab.com/ggujjula/cs373-idb/-/issues

Wiki Pages: https://gitlab.com/ggujjula/cs373-idb/-/wikis/home

Postman Documentation: https://documenter.getpostman.com/view/16325742/Tzm5JxbR
