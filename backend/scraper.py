import requests
import json
import sys
import random

from requests.models import CONTENT_CHUNK_SIZE

def scrapeData():
    politiciansendpoint = "https://api.propublica.org/congress/v1/117/senate/members.json" 
    billsendpoint = "https://api.propublica.org/congress/v1/bills/search.json?query=stimulus"
    orgendpoint = "https://www.opensecrets.org/api/?method=orgSummary&id=ORG_ID&apikey=OPENSEC_KEY&output=json"
    org_ids =  [
            "D000033563",
            "D000022008",
            "D000036161"
            ]
    orgendpoint = orgendpoint.replace("OPENSEC_KEY", sys.argv[1])
    propubheaders = {"X-API-Key" : sys.argv[2]}
    
    r = requests.get(politiciansendpoint, headers=propubheaders)
    #print(r.json())
    politiciansraw = r.json()["results"][0]["members"][0:3]
    politicians = list()
    for polinfo in politiciansraw:
        newentry = dict()
        newentry["id"] = polinfo["id"]
        newentry["crp_id"] = polinfo["crp_id"]
        newentry["first_name"] = polinfo["first_name"]
        newentry["last_name"] = polinfo["last_name"]
        newentry["party"] = polinfo["party"]
        newentry["state"] = polinfo["state"]
        newentry["title"] = polinfo["title"]
        politicians.append(newentry)
    #print(politicians)

    r = requests.get(billsendpoint, headers=propubheaders)
    #print(r.json())
    billsraw = r.json()["results"][0]["bills"][0:3]
    bills = list()
    for billinfo in billsraw:
        newentry = dict()
        newentry["bill_id"] = billinfo["bill_id"]
        newentry["title"] = billinfo["title"]
        newentry["congressdotgov_url"] = billinfo["congressdotgov_url"]
        newentry["primary_subject"] = billinfo["primary_subject"]
        newentry["latest_major_action"] = billinfo["latest_major_action"]
        newentry["sponsor_id"] = billinfo["sponsor_id"]
        bills.append(newentry)
    #print(bills)
         
    orgs = list()
    for org in org_ids:
        r = requests.get(orgendpoint.replace("ORG_ID", org))
        #print(r.json())
        orginfo = r.json()["response"]["organization"]["@attributes"]
        newentry = dict()
        newentry["orgid"] = orginfo["orgid"]
        newentry["orgname"] = orginfo["orgname"]
        newentry["dems"] = orginfo["dems"]
        newentry["repubs"] = orginfo["repubs"]
        newentry["lobbying"] = orginfo["lobbying"]
        newentry["cycle"] = orginfo["cycle"]
        orgs.append(newentry)
    #print(orgs)

    with open("politicians.json", "w") as f:
        f.write(json.dumps(politicians))

    with open("bills.json", "w") as f:
        f.write(json.dumps(bills))

    with open("orgs.json", "w") as f:
        f.write(json.dumps(orgs))


def scrapeSampleDB():
    senators_endpoint = "https://api.propublica.org/congress/v1/117/senate/members.json"
    house_endpoint = "https://api.propublica.org/congress/v1/117/house/members.json" 
    bills_endpoint = "https://api.propublica.org/congress/v1/117/house/bills/introduced.json?offset="
    propubheaders = {"X-API-Key" : sys.argv[2]}
    
    politicians = list()
    bills = list()
    organizations = list()
    pol_donators = dict()

    '''
    r = requests.get(senators_endpoint, headers=propubheaders)
    #print(r.json())
    senatorsraw = r.json()["results"][0]["members"]
    for polinfo in senatorsraw:
        if polinfo['crp_id'] and polinfo['id']:
            politicians.append(polinfo)

    r = requests.get(house_endpoint, headers=propubheaders)
    #print(r.json())
    houseraw = r.json()["results"][0]["members"]
    for polinfo in houseraw:
        if polinfo['crp_id'] and polinfo['id']:
            politicians.append(polinfo)
    #print(politicians)
    #'''


    '''    
    for page in range(0,1500,20):
        r = requests.get(bills_endpoint + str(page), headers=propubheaders)
    #    print(r.json())
        billsraw = r.json()["results"][0]["bills"]
        for billinfo in billsraw:
            if billinfo["primary_subject"] and billinfo["latest_major_action"] and billinfo["sponsor_id"]:
                bills.append(billinfo)
    #    print(bills)
    #'''


    #'''
    f = open("rawPoliticians.json", "r")
    pol = json.load(f)
    f.close()
    pol.sort(key=(lambda item: item['id']))

    f = open("pol-donators.json", "r")
    checked = json.load(f)
    f.close()
    checkedPols = checked.keys()
    rawCheckedOrgs = checked.values()
    checkedOrgs = set()
    for item in rawCheckedOrgs:
        for value in item:
            checkedOrgs.add(value)
    
    crp_id_lookup = "https://www.opensecrets.org/api/?method=candContrib&apikey=OPENSEC_KEY&output=json&cid="
    orgname_lookup = "https://www.opensecrets.org/api/?method=getOrgs&apikey=OPENSEC_KEY&output=json&org="
    org_lookup = "https://www.opensecrets.org/api/?method=orgSummary&apikey=OPENSEC_KEY&output=json&id="
    crp_id_lookup = crp_id_lookup.replace("OPENSEC_KEY", sys.argv[1])
    orgname_lookup = orgname_lookup.replace("OPENSEC_KEY", sys.argv[1])
    org_lookup = org_lookup.replace("OPENSEC_KEY", sys.argv[1])

    contributors = set()
    orgids = list()

    for politico in pol[0:69]:
        if not politico['id'] in checkedPols:
            #print(politico)
            r = requests.get(crp_id_lookup + str(politico["crp_id"]))
            #print(r.json())
            if r:
                contribs = r.json()["response"]["contributors"]["contributor"]
                donators = pol_donators[politico['id']] = list()
                #for idx in range(0,1):
                org_name = contribs[0]["@attributes"]["org_name"]
                donators.append(org_name)
                contributors.add(org_name)
    
    contributors = list(contributors)
    #random.shuffle(contributors)

    for contrib in contributors:
        if not contrib in checkedOrgs:
            r = requests.get(orgname_lookup + contrib)
            #print(r.json())
            if r:
                responses = r.json()["response"]["organization"]
                if not isinstance(responses, list):
                    responses = [responses] 
                for value in responses:
                    if value["@attributes"]["orgname"] == contrib:
                        orgids.append(value["@attributes"]["orgid"])

    for org in orgids:
        r = requests.get(org_lookup + org)
        if r:
            orginfo = r.json()["response"]["organization"]["@attributes"]
            organizations.append(orginfo)
    #'''

    #with open("rawPoliticians.json", "w") as f:
    #   f.write(json.dumps(politicians))
#
    #with open("rawBills.json", "w") as f:
    #   f.write(json.dumps(bills))
#
    with open("rawOrgs.json", "r+") as f:
        oldData = json.load(f)
        for item in organizations:
            oldData.append(item)
        f.seek(0)
        f.write(json.dumps(oldData))
        f.truncate()
#
    with open("pol-donators.json", "r+") as f:
        oldData = json.load(f)
        oldData.update(pol_donators)
        f.seek(0)
        f.write(json.dumps(oldData))
        f.truncate()




if __name__ == "__main__":
    #scrapeData()
    scrapeSampleDB()
