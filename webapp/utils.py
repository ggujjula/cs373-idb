from operator import indexOf
from flask.wrappers import Response
import json

noFilter = "no_filter"

#Reads in a JSON file and returns it.
def readFile(filename):
    datafile = open(filename)
    data = datafile.read()
    datafile.close()
    return data

#gets an argument called argName from request.args, and validates it by passing it to the function validateFunc
def getArg(args, argName, targetType, defaultVal, validateFunc):
    try:
        result = args.get(argName, default=defaultVal, type=targetType)
    except ValueError:
        result = invalid(args.get(argName), argName)
    if not validateFunc(result):
        result = invalid(args.get(argName), argName)
    return result

#searches for each attribute to filter on in filterArgList in request.args, checks if it's invalid or no filter, then applies the filter
def filter(query, args, filterArgList):
    for filter in filterArgList:
        arg = getArg(args, filter[0], type(filter[1]), filter[1], filter[3])
        if type(arg) is Response:
            return arg
        if not arg == noFilter:
            query = query.filter(filter[2](arg))
    return query

#checks if given int is greater than 0
def validateGreaterThanOrEqualToZero(val):
    return val >= 0

#check is given string is a bool string, cause SQLAlchemy won't pass me bools
def validateBoolArgs(val):
    return val in ["True", "False"] or val == noFilter

#gets a list of attributes and validates them value from request.args
def getOrders(args, validAttributes):
    sortingOrders = ["asc", "desc"]

    try:
        sortbyList = args.getlist("sortby", type=str)
    except ValueError:
        return invalid(args.get("sortby"), "sortby")
    try:
        ascOrDescList = args.getlist("sortorder", type=str)
        for direction in ascOrDescList:
            if not direction in sortingOrders:
                return invalid(args.get("sortorder"), "sortorder")
    except:
        return invalid(args.get("sortorder"), "sortorder")

    #can only pick a direction for the amount of parameters we have
    if len(ascOrDescList) > len(sortbyList):
        return invalid(len(ascOrDescList), "the number of sortorder parameters")

    result = []
    #convert strings given into actual database values
    for attribute in sortbyList:
        if attribute in validAttributes:
            result.append(validAttributes.get(attribute))
        else:
            return invalid(args.get("sortby"), "sortby")
    #invert certain sort values based on ordering in request
    for index, direction in enumerate(ascOrDescList):
        if direction == "desc":
            result[index] = result[index].desc()
    return result

#creates a flask Response stating that an invalid value was entered for a flask query field.
def invalid(arg, argName):
    # return jsonify({"error": f"{arg} is an invalid value for {argName}"})  
    return Response(
        json.dumps({"error": f"{arg} is an invalid value for {argName}"}),
        400
    )

