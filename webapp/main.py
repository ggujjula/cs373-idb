from flask import (
    request,
    jsonify,
    make_response,
    Response
)
from models import (
    app,
    db,
    Politician,
    Bill,
    Org
)
import subprocess
import utils
import politician_tools
import bill_tools
import organization_tools

@app.route("/")
def index():
    return "Politics in Review"

@app.route("/politicians/", methods=["GET"])
def getPoliticians():
    #Get base query
    polis = db.session.query(Politician)

    #Pagination (limit/offset) and Sorting (orders) and Filtering (politician.filter on polis)
    limit = utils.getArg(request.args, "limit", int, 25, utils.validateGreaterThanOrEqualToZero)
    offset = utils.getArg(request.args, "offset", int, 0, utils.validateGreaterThanOrEqualToZero)
    orders = utils.getOrders(request.args, politician_tools.validSortCriteria)
    polis = politician_tools.search(polis, request.args)
    polis = utils.filter(polis, request.args, politician_tools.filterArgs)
    

    #If anything is invalid it will have returned as a Bad Value Response
    if type(limit) is Response:
        return limit
    elif type(offset) is Response:
        return offset
    elif type(orders) is Response:
        return orders
    elif type(polis) is Response:
        return polis

    total_count = polis.count()
    #apply pagination and sorting
    polis = polis.order_by(*orders).limit(limit).offset(offset)

    #Prep to Return
    politiciansDict = [
        {
            "id": poli.id,
            "crp_id": poli.crp_id,
            "first_name": poli.first_name,
            "last_name": poli.last_name,
            "party": poli.party,
            "state": poli.state,
            "title": poli.title,
            "twitter": poli.twitter,
            "facebook": poli.facebook,
            "youtube": poli.youtube,
            "rss_url": poli.rss_url,
            "sponsored_bills": [str(bill.bill_id) for bill in poli.sponsored_bills],
            "orgs": [
            {
                "orgid" : str(org.orgid),
                "orgname" : str(org.orgname)
            } for org in poli.orgs
        ]
        }
        for poli in polis
    ]

    return jsonify(
        {
            "total": total_count,
            "size": len(politiciansDict),
            "results": politiciansDict,
            "limit": limit,
            "offset": offset
        }
    )

@app.route("/politicians/<givenID>", methods = ["GET"])
def getPoliticianID(givenID):
    poli = db.session.query(Politician).filter_by(id=givenID).first()

    if not poli:
        return utils.invalid(givenID, "id")

    politiciansDict = {
        "id": poli.id,
        "crp_id": poli.crp_id,
        "first_name": poli.first_name,
        "last_name": poli.last_name,
        "party": poli.party,
        "state": poli.state,
        "title": poli.title,
        "twitter": poli.twitter,
        "facebook": poli.facebook,
        "youtube": poli.youtube,
        "rss_url": poli.rss_url,
        "sponsored_bills": [str(bill.bill_id) for bill in poli.sponsored_bills],
        "orgs": [
            {
                "orgid" : str(org.orgid),
                "orgname" : str(org.orgname)
            } for org in poli.orgs
        ]
    }

    return jsonify(politiciansDict)

@app.route("/bills/", methods=["GET"])
def getBills():
    #Get base query
    bills = db.session.query(Bill)

    #Pagination (limit/offset) and Sorting (orders) and Filtering (politician.filter on polis)
    limit = utils.getArg(request.args, "limit", int, 25, utils.validateGreaterThanOrEqualToZero)
    offset = utils.getArg(request.args, "offset", int, 0, utils.validateGreaterThanOrEqualToZero)
    orders = utils.getOrders(request.args, bill_tools.validSortCriteria)
    bills = bill_tools.search(bills, request.args)
    bills = utils.filter(bills, request.args, bill_tools.filterArgs)
    
    #If anything is invalid it will have returned as a Bad Value Response
    if type(limit) is Response:
        return limit
    elif type(offset) is Response:
        return offset
    elif type(orders) is Response:
        return orders
    elif type(bills) is Response:
        return bills

    total_count = bills.count()
    #apply pagination and sorting
    bills = bills.order_by(*orders).limit(limit).offset(offset)

    #Prep to Return
    billsDict = [
        {
            "bill_id": bill.bill_id,
            "title": bill.title,
            "congressdotgov_url": bill.congressdotgov_url,
            "primary_subject": bill.primary_subject,
            "latest_major_action": bill.latest_major_action,
            "sponsor_id": bill.sponsor_id,
            "pdf_url": bill.pdf_url,
            "orgs": [
                {
                    "orgid" : str(org.orgid),
                    "orgname" : str(org.orgname)
                } for org in bill.orgs
            ]
        }
        for bill in bills
    ]

    return jsonify(
        {
            "total": total_count,
            "size": len(billsDict),
            "results": billsDict,
            "limit": limit,
            "offset": offset
        }
    )

@app.route("/bills/<givenID>", methods=["GET"])
def getBillID(givenID):
    bill = db.session.query(Bill).filter_by(bill_id=givenID).first()

    if not bill:
        return utils.invalid(givenID, "bill_id")

    billDict = {
        "bill_id": bill.bill_id,
        "title": bill.title,
        "congressdotgov_url": bill.congressdotgov_url,
        "primary_subject": bill.primary_subject,
        "latest_major_action": bill.latest_major_action,
        "sponsor_id": bill.sponsor_id,
        "pdf_url": bill.pdf_url,
        "orgs": [
            {
                "orgid" : str(org.orgid),
                "orgname" : str(org.orgname)
            } for org in bill.orgs
        ]
    }

    return jsonify(billDict)

@app.route("/organizations/", methods=["GET"])
def getOrganizations():
    #Get base query
    orgs = db.session.query(Org)

    #Pagination (limit/offset) and Sorting (orders) and Filtering (politician.filter on polis)
    limit = utils.getArg(request.args, "limit", int, 25, utils.validateGreaterThanOrEqualToZero)
    offset = utils.getArg(request.args, "offset", int, 0, utils.validateGreaterThanOrEqualToZero)
    orders = utils.getOrders(request.args, organization_tools.validSortCriteria)
    orgs = organization_tools.search(orgs, request.args)
    orgs = utils.filter(orgs, request.args, organization_tools.filterArgs)
    
    #If anything is invalid it will have returned as a Bad Value Response
    if type(limit) is Response:
        return limit
    elif type(offset) is Response:
        return offset
    elif type(orders) is Response:
        return orders
    elif type(orgs) is Response:
        return orgs
    
    total_count = orgs.count()
    #apply pagination and sorting
    orgs = orgs.order_by(*orders).limit(limit).offset(offset)

    #Prep to Return
    orgsDict = [
        {
            "orgid": org.orgid,
            "orgname": org.orgname,
            "dems": org.dems,
            "repubs": org.repubs,
            "lobbying": org.lobbying,
            "cycle": org.cycle,
            "bills": [str(bill.bill_id) for bill in org.bills],
            "politicians": [
                {
                    "id" : str(poli.id),
                    "name" : str(poli.first_name) + str(poli.last_name)
                } for poli in org.politicians
            ]
        }
        for org in orgs
    ]

    return jsonify(
        {
            "total": total_count,
            "size": len(orgsDict),
            "results": orgsDict,
            "limit": limit,
            "offset": offset
        }
    )

@app.route("/organizations/<givenID>", methods=["GET"])
def getOrganizationID(givenID):
    org = db.session.query(Org).filter_by(orgid=givenID).first()
    
    if not org:
        return utils.invalid(givenID, "orgid")

    orgDict = {
        "orgid": org.orgid,
        "orgname": org.orgname,
        "dems": org.dems,
        "repubs": org.repubs,
        "lobbying": org.lobbying,
        "cycle": org.cycle,
        "bills": [str(bill.bill_id) for bill in org.bills],
        "politicians": [
            {
                "id" : str(poli.id),
                "name" : str(poli.first_name) + " " + str(poli.last_name)
            } for poli in org.politicians
        ]
    }

    return jsonify(orgDict)

@app.route("/test/", methods = ["GET"])
def runtests():
    p = subprocess.run(['python3', 'tests.py', '--verbose'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, encoding='utf8')
    r = make_response(p.stdout, 200)
    r.mimetype = "text/plain"
    return r

if __name__ == "__main__":
    app.run()
