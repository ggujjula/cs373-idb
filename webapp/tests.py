from unittest import main, TestCase, expectedFailure

from werkzeug.datastructures import ImmutableDict, ImmutableMultiDict
from app import db, Politician, Bill, Org
from flask import Response
import utils, politician_tools, bill_tools, organization_tools

class BackendTests(TestCase):

  #Written by Gautham
  def test_db_commit(self):
    db.session.commit()

  #Written by Gautham
  def test_query_politician_list(self):
    polList = db.session.query(Politician).limit(10).all()
    self.assertEqual(len(polList), 10)

  #Written by Gautham
  def test_query_bill_list(self):
    billList = db.session.query(Bill).limit(10).all()
    self.assertEqual(len(billList), 10)

  #Written by Gautham
  def test_query_org_list(self):
    orgList = db.session.query(Org).limit(10).all()
    self.assertEqual(len(orgList), 10)

  #Written by Gautham
  def test_query_politician_byid(self):
    pol = db.session.query(Politician).get('B001230')
    self.assertEqual(pol.id, 'B001230')
    self.assertEqual(pol.first_name, 'Tammy')
    self.assertEqual(pol.last_name, 'Baldwin')

  #Written by Gautham
  def test_query_bill_byid(self):
    bill = db.session.query(Bill).get('hr3684-117')
    self.assertEqual(bill.bill_id, 'hr3684-117')
    self.assertEqual(bill.sponsor_id, 'D000191')

  #Written by Gautham
  def test_query_org_byid(self):
    org = db.session.query(Org).get('D000045868')
    self.assertEqual(org.orgid, 'D000045868')
    self.assertEqual(org.orgname, 'Barrick Gold Corp')

  #Written by Gautham
  def test_query_politicians_offset(self):
    list1 = db.session.query(Politician).order_by(Politician.id).limit(10).offset(0).all()
    list2 = db.session.query(Politician).order_by(Politician.id).limit(10).offset(1).all()
    self.assertEqual(len(list1), 10)
    self.assertEqual(len(list2), 10)
    self.assertEqual(list1[1:], list2[:-1])
    self.assertEqual(list1[1], list2[0])

  #Written by Gautham
  def test_query_bills_offset(self):
    list1 = db.session.query(Bill).order_by(Bill.bill_id).limit(10).offset(0).all()
    list2 = db.session.query(Bill).order_by(Bill.bill_id).limit(10).offset(1).all()
    self.assertEqual(len(list1), 10)
    self.assertEqual(len(list2), 10)
    self.assertEqual(list1[1:], list2[:-1])
    self.assertEqual(list1[1], list2[0])

  #Written by Gautham
  def test_query_orgs_offset(self):
    list1 = db.session.query(Org).order_by(Org.orgid).limit(10).offset(0).all()
    list2 = db.session.query(Org).order_by(Org.orgid).limit(10).offset(1).all()
    self.assertEqual(len(list1), 10)
    self.assertEqual(len(list2), 10)
    self.assertEqual(list1[1:], list2[:-1])
    self.assertEqual(list1[1], list2[0])

  #Written by Gautham
  def test_insert_politician(self):
    obj = Politician(\
            id          = "abc",\
            crp_id      = "abc",\
            first_name  = "abc",\
            last_name   = "abc",\
            party       = "abc",\
            state       = "abc",\
            title       = "abc"\
          )
    db.session.add(obj)
    db.session.commit()
    qobj = db.session.query(Politician).get('abc') 
    self.assertEqual(qobj.id, obj.id)
    db.session.query(Politician).filter_by(id='abc').delete()
    db.session.commit()

  #Written by Gautham
  def test_insert_bill(self):
    obj = Bill(\
            bill_id                   = "abc",\
            title                = "abc",\
            congressdotgov_url   = "abc",\
            primary_subject      = "abc",\
            latest_major_action  = "abc",\
            sponsor_id           = "abc",\
          )
    db.session.add(obj)
    db.session.commit()
    qobj = db.session.query(Bill).get('abc') 
    self.assertEqual(qobj.bill_id, obj.bill_id)
    db.session.query(Bill).filter_by(bill_id='abc').delete()
    db.session.commit()

  #Written by Gautham
  def test_insert_org(self):
    obj = Org(\
            orgid       = "abc",\
            orgname     = "abc",\
            dems        = 0,\
            repubs      = 0,\
            lobbying    = 0,\
            cycle       = 0,\
          )
    db.session.add(obj)
    db.session.commit()
    qobj = db.session.query(Org).get('abc') 
    self.assertEqual(qobj.orgid, obj.orgid)
    db.session.query(Org).filter_by(orgid='abc').delete()
    db.session.commit()

  #Written by Ryan
  def test_utils_get_orders_invalid(self):
    testDict = ImmutableMultiDict([("sortby", "first_name"), \
                                    ("sortby", "last_name"), \
                                    ("sortorder", "asc"), \
                                    ("sortorder", "asc"), \
                                    ("sortorder", "asc")
                                ])
    orders = utils.getOrders(testDict, politician_tools.validSortCriteria)
    self.assertEqual(type(orders), Response)

  #Written by Ryan
  def test_utils_get_orders(self):
    query = db.session.query(Politician)
    correct = [Politician.first_name.desc(), Politician.last_name.desc(), Politician.state]
    correctResult = query.order_by(*correct).first()
    testDict = ImmutableMultiDict([("sortby", "first_name"), \
                                    ("sortby", "last_name"), \
                                    ("sortby", "state"), \
                                    ("sortorder", "desc"), \
                                    ("sortorder", "desc"), \
                                    ("sortorder", "asc")
                                ])
    orders = utils.getOrders(testDict, politician_tools.validSortCriteria)
    result = query.order_by(*orders).first()
    self.assertEqual(result, correctResult)

if __name__ == "__main__":
  main()
