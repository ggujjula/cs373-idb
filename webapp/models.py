from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
import time
import os
from flask_cors import CORS

USER = 'postgres'
PASSWORD = 'chickensandwich86'
ADDR_PORT = 'localhost:5432'
DB_NAME = 'idb'

app = Flask(__name__)
CORS(app)
app.config['SQLALCHEMY_DATABASE_URI'] = \
os.environ.get("DB_STRING",f'postgresql://{USER}:{PASSWORD}@{ADDR_PORT}/{DB_NAME}')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True  # To suppress a warning message

db = None
db_connected = False
while(not db_connected):
    try:
        db = SQLAlchemy(app)
        db_connected = True
    except(OperationalError):
        print("Error connecting to db, trying again in 5 seconds ...")
        time.sleep(5)

pol_org_table = db.Table('pol_org_table',
    db.Column('pol_id', db.String(16), db.ForeignKey('Politician.id'), primary_key=True),
    db.Column('org_id', db.String(16), db.ForeignKey('Org.orgid'), primary_key=True)
)
"""
A link table for the politician-org many to many relationship
"""

bill_org_table = db.Table('bill_org_table',
    db.Column('bill_id', db.String(16), db.ForeignKey('Bill.bill_id'), primary_key=True),
    db.Column('org_id', db.String(16), db.ForeignKey('Org.orgid'), primary_key=True)
)
"""
A link table for the bill-org many to many relationship
"""

class Politician(db.Model):
    """
        The model for a Politician entry in our database.
        Has 7 attributes:
        id: Unique ID that entry is indexed by. Same as ProPublica ID.
        crp_id: OpenSecrets ID
        first_name: First name of politician.
        last_name: Last name of politician.
        party: Political party the politician is a part of.
        state: US state the politician represents.
        title: Politician's title in House/Senate.
        twitter: the twitter handle of the politician, hidden     
        facebook: the facebook username of the politician, hidden       
        youtube: the youtube username of the politician, hidden        
        rss_url: an rss feed with media about the politician's actions          
        sponsored_bills: a list of bills sponsored by the politician 
        orgs: a list of the top ten orgs who have donated to the politician          
    """
    __tablename__ = 'Politician'
    id              = db.Column(db.String(16), primary_key=True)
    crp_id          = db.Column(db.String(16), unique=True, nullable=False)
    first_name      = db.Column(db.String(64), nullable=False)
    last_name       = db.Column(db.String(64), nullable=False)
    party           = db.Column(db.String(64), nullable=False)
    state           = db.Column(db.String(64), nullable=False)
    title           = db.Column(db.String(64), nullable=False)
    twitter         = db.Column(db.String(64), nullable=True)
    facebook        = db.Column(db.String(64), nullable=True)
    youtube         = db.Column(db.String(64), nullable=True)
    rss_url         = db.Column(db.String(128), nullable=True)
    sponsored_bills = db.relationship('Bill', backref='politician', lazy=True, cascade='all,delete')
    orgs            = db.relationship('Org', secondary=pol_org_table, lazy='subquery', backref=db.backref('politicians', lazy=True), cascade='all,delete')

    def __repr__(self):
        """
            Returns a string that uniquely identifies the Politician model.
        """ 
        return '<Politician %s>' % self.id

class Bill(db.Model):
    """
        The model for a Bill entry in our database.
        Has 6 attributes:
        bill_id: Unique ID that entry is indexed by. Same as ProPublica ID.
        title: Full bill title.
        congressdotgov_url: URL to congress.gov which provides more detail.
        primary_subject: Bill category
        latest_major_action: Current status of the bill.
        sponsor_id: ID of the politician that introduced the bill.
        pdf_url: a url directly to a pdf file of the bill. hidden
        sponsor_id_foreign: a value required by the one to many politician-bill relation, hidden
        orgs: a list of all orgs who support the bill's sponsor
    """
    __tablename__ = 'Bill'
    bill_id              = db.Column(db.String(16), primary_key=True)
    title                = db.Column(db.String(2048), nullable=False)
    congressdotgov_url   = db.Column(db.String(128), unique=True, nullable=False)
    primary_subject      = db.Column(db.String(64), nullable=False)
    latest_major_action  = db.Column(db.String(512), nullable=False)
    sponsor_id           = db.Column(db.String(16), nullable=False)
    pdf_url              = db.Column(db.String(128), nullable=True)
    sponsor_id_foreign   = db.Column(db.String(16), db.ForeignKey('Politician.id'), nullable=True)
    orgs                 = db.relationship('Org', secondary=bill_org_table, lazy='subquery', backref=db.backref('bills', lazy=True), cascade='all,delete')

    def __repr__(self):
        """
            Returns a string that uniquely identifies the Bill model.
        """ 
        return '<Bill %s>' % self.bill_id

class Org(db.Model):
    """
        The model for a Politician entry in our database.
        Has 6 attributes:
        orgid: The OpenSecrets ID of the organization.
        orgname: The name of the organization.
        dems: How much funding the organization provided to Democrats.
        repubs: How much funding the organization provided to Republicans.
        lobbying: How much funding the organization spent in total on lobbying in the last election cycle.
        cycle: The last election cycle that there is data for.
        bills: an attribute added by the many to many org-bill relation
        politicians: an attribute added by the many to many politician-org relation

    """
    __tablename__ = 'Org'
    orgid       = db.Column(db.String(16), primary_key=True)
    orgname     = db.Column(db.String(64), unique=True, nullable=False)
    dems        = db.Column(db.Integer, nullable=False)
    repubs      = db.Column(db.Integer, nullable=False)
    lobbying    = db.Column(db.Integer, nullable=False)
    cycle       = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        """
            Returns a string that uniquely identifies the Org model.
        """ 
        return '<Org %s>' % self.orgid

