from sys import maxsize
from models import Bill, Org, Politician
from sqlalchemy import func, or_, cast, String
from utils import (
    noFilter,
    validateBoolArgs,
    validateGreaterThanOrEqualToZero
)

#sorting
validSortCriteria = {
    "id": Org.orgid,
    "name": Org.orgname,
    "dems": Org.dems,
    "repubs": Org.repubs,
    "lobbying": Org.lobbying,
    "cycle": Org.cycle,
}

#filtering
filterArgs = {
    ("minDems", 0, (lambda givenMinDems: Org.dems>=givenMinDems), validateGreaterThanOrEqualToZero),
    ("maxDems", maxsize, (lambda givenMaxDems: Org.dems<=givenMaxDems), validateGreaterThanOrEqualToZero),
    ("minRepubs", 0, (lambda givenMinRepubs: Org.repubs>=givenMinRepubs), validateGreaterThanOrEqualToZero),
    ("maxRepubs", maxsize, (lambda givenMaxRepubs: Org.repubs<=givenMaxRepubs), validateGreaterThanOrEqualToZero),
    ("minLobby", 0, (lambda givenMinLobby: Org.lobbying>=givenMinLobby), validateGreaterThanOrEqualToZero),
    ("maxLobby", maxsize, (lambda givenMaxLobby: Org.lobbying<=givenMaxLobby), validateGreaterThanOrEqualToZero),
    ("hasBills", noFilter, (lambda givenHasBills: Org.bills.any() if givenHasBills == "True" else ~Org.bills.any()), validateBoolArgs),
    ("hasPoliticians", noFilter, (lambda givenHasPoliticians: Org.politicians.any() if givenHasPoliticians == "True" else ~Org.politicians.any()), validateBoolArgs)
}

#searching
def search(query, args):
    searchTerms = args.get("q", type=str)
    
    if searchTerms:
        searchTerms = searchTerms.split(" ")
        for searchTerm in searchTerms:
            searchTerm = searchTerm.strip().lower()
            queries = []

            queries.append(func.lower(Org.orgid).contains(searchTerm))
            queries.append(func.lower(Org.orgname).contains(searchTerm))
            queries.append(cast(Org.dems, String).contains(searchTerm))
            queries.append(cast(Org.repubs, String).contains(searchTerm))
            queries.append(cast(Org.lobbying, String).contains(searchTerm))
            queries.append(cast(Org.cycle, String).contains(searchTerm))

            query = query.filter(or_(*queries))
    return query