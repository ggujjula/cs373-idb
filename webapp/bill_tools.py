from models import Bill, Org
from sqlalchemy import func, or_
from utils import (
    noFilter,
    validateBoolArgs
)

#sorting
validSortCriteria = {
    "id": Bill.bill_id,
    "subject": Bill.primary_subject,
    "sponsor": Bill.sponsor_id
}

#filtering
validSubjects = {
    'Congress': 'Congress',
    'Native': 'Native Americans',
    'Economics': 'Economics and Public Finance',
    'Environmental': 'Environmental Protection',
    'Energy': 'Energy',
    'Armed': 'Armed Forces and National Security',
    'Health': 'Health',
    'Crime': 'Crime and Law Enforcement',
    'Civil': 'Civil Rights and Liberties, Minority Issues',
    'International': 'International Affairs',
    'Government': 'Government Operations and Politics',
    'Taxation': 'Taxation',
    'Families': 'Families',
    'Science': 'Science, Technology, Communications',
    'Labor': 'Labor and Employment',
    'Emergency': 'Emergency Management',
    'Education': 'Education',
    'Public': 'Public Lands and Natural Resources',
    'Transportation': 'Transportation and Public Works',
    'Social': 'Social Welfare',
    'Commerce': 'Commerce',
    'Law': 'Law',
    'Housing': 'Housing and Community Development',
    'Finance': 'Finance and Financial Sector',
    'Immigration': 'Immigration',
    'Foreign': 'Foreign Trade and International Finance',
    'Arts': 'Arts, Culture, Religion',
    'Agriculture': 'Agriculture and Food',
    'Water': 'Water Resources Development'
}

def validateSubject(val):
    return val in validSubjects.keys() or val == noFilter

filterArgs = [
    ("subject", noFilter, (lambda givenSubject: Bill.primary_subject==validSubjects[givenSubject]), validateSubject),
    ("hasOrgs", noFilter, (lambda givenHasOrgs: Bill.orgs.any() if givenHasOrgs == "True" else ~Bill.orgs.any()), validateBoolArgs)
]

#searching
def search(query, args):
    searchTerms = args.get("q", type=str)
    if searchTerms:
        searchTerms = searchTerms.split(" ")
        for searchTerm in searchTerms:
            searchTerm = searchTerm.strip().lower()
            queries = []

            queries.append(func.lower(Bill.bill_id).contains(searchTerm))
            queries.append(func.lower(Bill.title).contains(searchTerm))
            queries.append(func.lower(Bill.congressdotgov_url).contains(searchTerm))
            queries.append(func.lower(Bill.primary_subject).contains(searchTerm))
            queries.append(func.lower(Bill.latest_major_action).contains(searchTerm))
            queries.append(func.lower(Bill.sponsor_id).contains(searchTerm))
            
            query = query.filter(or_(*queries))
    return query