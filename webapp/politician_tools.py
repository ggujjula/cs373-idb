from models import Politician, Bill, Org
from sqlalchemy import func, or_
from utils import (
    noFilter,
    getArg,
    validateBoolArgs
)

#sorting
validSortCriteria = {
    "id": Politician.id,
    "crp_id": Politician.crp_id,
    "first_name": Politician.first_name,
    "last_name": Politician.last_name,
    "party": Politician.party,
    "state": Politician.state,
    "title": Politician.title
}

#filtering
validParties = ['D', 'R', 'ID']
validStates = ['WI', 'WY', 'CO', 'TN', 'CT', 'MO', 'NJ', 'AR', 'OH', 'NC', 'WA', 'MD', 'DE', 'PA', 'ME', 'TX', 'ND', 'ID', 'MT', 'IL', 'CA', 'NE', 'NY', 'SC', 'IA', 'NM', 'HI', 'OK', 'AZ', 'MN', 'VT', 'UT', 'WV', 'MA', 'OR', 'KS', 'AK', 'KY', 'MI', 'RI', 'FL', 'NH', 'AL', 'SD', 'VA', 'MS', 'IN', 'NV', 'GA', 'DC', 'LA', 'MP']
validTitles = {
    'S1C': 'Senator, 1st Class',
    'S2C': 'Senator, 2nd Class',
    'S3C': 'Senator, 3rd Class',
    'rep': 'Representative',
    'del': 'Delegate'
}

def validateParty(val):
    return val in validParties or val == noFilter

def validateState(val):
    return val in validStates or val == noFilter

def validateTitle(val):
    return val in validTitles.keys() or val == noFilter

filterArgs = [
    ("party", noFilter, (lambda givenParty: Politician.party==givenParty), validateParty),
    ("state", noFilter, (lambda givenState: Politician.state==givenState), validateState),
    ("title", noFilter, (lambda givenTitle: Politician.title==validTitles[givenTitle]), validateTitle),
    ("hasBills", noFilter, (lambda givenHasBills: Politician.sponsored_bills.any() if givenHasBills == "True" else ~Politician.sponsored_bills.any()), validateBoolArgs),
    ("hasOrgs", noFilter, (lambda givenHasOrgs: Politician.orgs.any() if givenHasOrgs == "True" else ~Politician.orgs.any()), validateBoolArgs)
]

#searching
def validateSearchTerm(val):
    return type(val) == str

def search(query, args):
    searchTerms = args.get("q", type=str)
    if searchTerms:
        searchTerms = searchTerms.split(" ")
        for searchTerm in searchTerms:
            searchTerm = searchTerm.strip().lower()
            queries = []

            queries.append(func.lower(Politician.id).contains(searchTerm))
            queries.append(func.lower(Politician.crp_id).contains(searchTerm))
            queries.append(func.lower(Politician.first_name).contains(searchTerm))
            queries.append(func.lower(Politician.last_name).contains(searchTerm))
            queries.append(func.lower(Politician.party).contains(searchTerm))
            queries.append(func.lower(Politician.state).contains(searchTerm))
            queries.append(func.lower(Politician.title).contains(searchTerm))
            
            query = query.filter(or_(*queries))
    return query