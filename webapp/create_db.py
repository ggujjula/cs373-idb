import json
from models import app, db, Politician, Bill, Org

def db_populate():
    with open('./rawPoliticians.json', 'r') as f:
        politicians = json.load(f)
    with open('./rawBills.json', 'r') as f:
        bills = json.load(f)
    with open('./pol-donators.json', 'r') as f:
        poldonators = json.load(f)
    with open('./rawOrgs.json', 'r') as f:
        orgs = json.load(f)

    toCommitPols = list()
    toCommitBills = list()
    toCommitOrgs = list()

    for item in politicians:
        if item['twitter_account'] and item['youtube_account']:
            toCommitPols.append( Politician(
                id = item['id'],
                crp_id = item['crp_id'],
                first_name = item['first_name'],
                last_name = item['last_name'],
                party = item['party'],
                state = item['state'],
                title = item['title'],
                twitter = item['twitter_account'],
                facebook = item['facebook_account'],
                youtube = item['youtube_account'],
                rss_url = item['rss_url']
            ))

    for item in bills:
        toCommitBills.append( Bill(
            bill_id = item['bill_id'],
            title = item['title'],
            congressdotgov_url = item['congressdotgov_url'],
            primary_subject = item['primary_subject'],
            latest_major_action = item['latest_major_action'],
            sponsor_id = item['sponsor_id'],
            pdf_url = item['gpo_pdf_uri'],
        ))

    for item in orgs:
        toCommitOrgs.append( Org(
            orgid = item['orgid'],
            orgname = item['orgname'],
            dems = item['dems'],
            repubs = item['repubs'],
            lobbying = item['lobbying'],
            cycle = item['cycle']
        ))

    db.session.add_all(toCommitPols)
    db.session.add_all(toCommitBills)
    db.session.add_all(toCommitOrgs)
    db.session.commit()

    for bill in toCommitBills:
      for pol in toCommitPols:
        if bill.sponsor_id == pol.id:
          pol.sponsored_bills.append(bill)

    for pol in toCommitPols:
      if pol.id in poldonators:
        for donatorname in poldonators[pol.id]:
          for org in toCommitOrgs:
            if org.orgname == donatorname:
              pol.orgs.append(org)

    for pol in toCommitPols:
      for bill in pol.sponsored_bills:
        for org in pol.orgs:
          bill.orgs.append(org)
 
    db.session.commit()

def db_reset():
    db.drop_all()
    db.create_all()

if __name__ == "__main__":
    db_reset()
    db_populate()
    print("Populated database")
